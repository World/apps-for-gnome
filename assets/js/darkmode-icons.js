var darkModeMediaQuery = window.matchMedia('(prefers-color-scheme: dark)');
handleDarkmode(darkModeMediaQuery);

darkModeMediaQuery.addEventListener('change', handleDarkmode);

function handleDarkmode(e){
  const sizes = ['16x16', '32x32'];
  var active = e.matches;
  var favicons = document.querySelectorAll('link[rel="icon"]');

  favicons.forEach(favicon => {
      if (favicon.sizes && sizes.includes(favicon.sizes.value)) {
      var href = favicon.href;
      if (active) {
        href = href.replace('-light.png', '-dark.png');
      } else {
        href = href.replace('-dark.png', '-light.png');
      }
      favicon.href = href;
    }
  });
}
