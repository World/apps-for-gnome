#!/bin/bash

rm -rf cache/gnome-build-meta
mkdir -p cache
cd cache
git clone --depth=1 https://gitlab.gnome.org/GNOME/gnome-build-meta.git