#!/usr/bin/python3

import os
import sys
import shutil
import glob
import subprocess
from pathlib import Path


def main():
    non_flatpak = Path("non-flatpak").resolve()
    shutil.rmtree(non_flatpak, ignore_errors=True)
    non_flatpak.mkdir()
    os.chdir(non_flatpak)

    prefix = non_flatpak / "local"

    os.mkdir("metainfo")
    os.mkdir("icons")

    apps = [
        ("core", "console", "org.gnome.Console"),
        ("core", "gnome-control-center", "org.gnome.Settings"),
        ("core", "gnome-disk-utility", "org.gnome.DiskUtility"),
        ("core", "gnome-software", "org.gnome.Software"),
        ("core", "gnome-system-monitor", "org.gnome.SystemMonitor"),
        ("core", "gnome-tour", "org.gnome.Tour"),
        ("core", "nautilus", "org.gnome.Nautilus"),
        ("core", "sysprof", "org.gnome.Sysprof"),
        ("core", "yelp", "yelp"),
        ("incubator", "papers", "org.gnome.Papers"),
        ("incubator", "showtime", "org.gnome.Showtime"),
    ]

    errors = 0

    for category, module, app_id in apps:
        print(app_id)
        try:
            process_app(prefix, category, module, app_id)
        except Exception as exception:
            print(f"\nCouldn't process {app_id}: {exception}\n")
            errors += 1

    print("\nNumber of errors:", errors)

    sys.exit(bool(errors))


def process_app(prefix, category, module, app_id):
    if category == "incubator":
        subprocess.run(
            [
                "git",
                "clone",
                "--depth=1",
                f"https://gitlab.gnome.org/GNOME/Incubator/{module}.git",
            ],
            check=True,
        )

    else:
        subprocess.run(
            [
                "git",
                "clone",
                "--depth=1",
                f"https://gitlab.gnome.org/GNOME/{module}.git",
            ],
            check=True,
        )

    os.chdir(module)

    icon_name = app_id

    # MANUAL FIXES
    if module == "gnome-software":
        shutil.rmtree("plugins", ignore_errors=True)
    if module == "gnome-control-center":
        icon_name = "org.gnome.Settings"
    if module == "sysprof":
        icon_name = "org.gnome.Sysprof"
    if module == "gnome-system-monitor":
        icon_name = "org.gnome.SystemMonitor"
    if module == "yelp":
        icon_name = "org.gnome.Yelp"

    for app_icon in Path().rglob(f"{ icon_name }.svg"):
        shutil.copyfile(app_icon, f"../icons/{ app_id }.svg")

    for app_icon in Path().rglob(f"{ icon_name }-symbolic.svg"):
        shutil.copyfile(app_icon, f"../icons/{ app_id }-symbolic.svg")

    metainfo_files = glob.glob("**/*.metainfo.xml*", recursive=True) + glob.glob(
        "**/*.appdata.xml*", recursive=True
    )
    metainfo_file = min(metainfo_files, key=len)

    print(f"Selected {metainfo_file} from {metainfo_files}")

    meson = f"""project('{ module }')

        i18n = import('i18n')

        conf_data = configuration_data({{
            'app-id': '{ app_id }',
            'appid': '{ app_id }',
            'app_id': '{ app_id }',
            'application_id': '{ app_id }',
            'APP_ID': '{ app_id }',
            'KGX_APPLICATION_ID_RAW': '{ app_id }',
            'gettext-package': '{ module }',
            # Nautilus
            'release-version': '?',
        }})

        i18n.merge_file (
          input: configure_file(
            input: '{ metainfo_file }',
            output: 'tmp.metainfo.xml',
            configuration: conf_data
          ),
          output: '{ app_id }.metainfo.xml',
          po_dir: 'po/',
          install: true,
          install_dir: get_option('datadir') / 'metainfo'
        )
    """

    with open("meson.build", "w") as f:
        f.write(meson)

    subprocess.run(
        ["meson", "setup", "--prefix", f"{ prefix }", "builddir"], check=True
    )
    subprocess.run(["ninja", "install", "-C", "builddir"], check=True)
    os.chdir("..")

    subprocess.run(
        [
            "appstreamcli",
            "convert",
            "--format=xml",
            f"{ prefix }/share/metainfo/{ app_id }.metainfo.xml",
            f"metainfo/{ app_id }.xml",
        ],
        check=True,
    )


if __name__ == "__main__":
    main()
