use askama::Result;
use dynfmt::Format;

use crate::{api, i18n};

/// Translate string
pub fn t(s: &str) -> askama::Result<String> {
    i18n::add_string(s, Default::default());
    Ok(i18n::translate(s))
}

pub fn tc(s: &str, comment: &str) -> askama::Result<String> {
    i18n::add_string_with_comment(s, comment);
    Ok(i18n::translate(s))
}

pub fn f(s: &str, args: impl dynfmt::FormatArgs) -> Result<String> {
    let result = dynfmt::SimpleCurlyFormat.format(s, args);

    match result {
        Ok(formatted) => Ok(formatted.to_string()),
        Err(err) => {
            warn!("format: {} in {:?}", err, s);
            Ok(s.to_string())
        }
    }
}

pub fn select_language<'a>(
    app: &'a api::App,
    lang: &str,
) -> askama::Result<Option<&'a api::TranslationStats>> {
    Ok(app
        .languages
        .iter()
        .find(|language| language.locale == lang.replace('-', "_")))
}

pub fn latest_release(app: &api::App, devel: bool) -> askama::Result<Option<&api::Release>> {
    Ok(app
        .releases
        .iter()
        .find(|release| devel || release.kind == "stable"))
}

pub fn is_containing(s: impl AsRef<str>, search: impl AsRef<str>) -> askama::Result<bool> {
    Ok(s.as_ref().contains(search.as_ref()))
}

pub trait Containing {
    fn is_not_empty(&self) -> askama::Result<bool>;
}

impl<T> Containing for &Vec<T> {
    fn is_not_empty(&self) -> askama::Result<bool> {
        Ok(!self.is_empty())
    }
}

impl Containing for &String {
    fn is_not_empty(&self) -> askama::Result<bool> {
        Ok(!self.is_empty())
    }
}

impl Containing for &Option<String> {
    fn is_not_empty(&self) -> askama::Result<bool> {
        Ok(self.as_ref().map(|x| !x.is_empty()).unwrap_or_default())
    }
}

pub fn is_not_empty<T: Containing>(v: T) -> askama::Result<bool> {
    v.is_not_empty()
}

pub fn option_to_string<T: ToString>(s: &Option<T>) -> askama::Result<String> {
    Ok(s.as_ref().map(|x| x.to_string()).unwrap_or_default())
}

pub fn replace(
    s: impl AsRef<str>,
    from: impl AsRef<str>,
    to: impl AsRef<str>,
) -> askama::Result<String> {
    Ok(s.as_ref().replace(from.as_ref(), to.as_ref()))
}

pub fn striptags(input: &str) -> askama::Result<String> {
    Ok(input
        .chars()
        .fold((String::new(), false), |(mut s, in_tag), c| {
            if c == '>' {
                (s, false)
            } else if c == '<' || in_tag {
                (s, true)
            } else {
                s.push(c);
                (s, in_tag)
            }
        })
        .0)
}
