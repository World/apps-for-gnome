use std::fs::File;
use std::io::BufReader;
use std::path::{Path, PathBuf};
use std::sync::{LazyLock, RwLock};

use chrono::Utc;
use color_eyre::Help;
use indexmap::IndexMap;
use rand::prelude::*;
use serde::{Deserialize, Serialize};

use crate::api::*;

static DOWNLOAD_CACHE_DIR: LazyLock<RwLock<PathBuf>> =
    LazyLock::new(|| RwLock::new("cache".into()));

pub fn download_cache_dir() -> PathBuf {
    let dir = DOWNLOAD_CACHE_DIR.read().unwrap().clone();
    std::fs::create_dir_all(&dir).unwrap();
    dir
}

pub fn set_download_cache_dir(path: PathBuf) {
    *DOWNLOAD_CACHE_DIR.write().unwrap() = path;
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct App {
    pub gnome: String,
    pub id: AppId,
    /// Used in short URIs, last segment of app id in CamelCase
    pub page_id: String,
    pub nightly_id: Option<AppId>,
    pub name: String,
    pub name_ord: u64,
    pub summary: String,
    pub description: String,
    pub project_license: String,
    pub metadata_license: String,
    pub screenshots_light: Vec<Screenshot>,
    pub screenshots_dark: Option<Vec<Screenshot>>,
    pub url_homepage: Option<String>,
    pub url_bugtracker: Option<String>,
    pub url_help: Option<String>,
    pub url_donation: Option<String>,
    pub url_translate: Option<String>,
    pub url_contact: Option<String>,
    pub url_vcs_browser: Option<String>,
    pub url_contribute: Option<String>,
    pub repo_url: String,
    pub repo_name: String,
    pub categories: Vec<String>,
    pub releases: Vec<Release>,
    pub languages: Vec<TranslationStats>,
    pub dl_module: Option<String>,
    pub dl_strings: Option<u64>,
    pub members: Vec<Person>,
    pub key_colors: Option<String>,
    pub brand_color_light: String,
    pub brand_color_dark: String,
    pub text_color_light: String,
    pub text_color_dark: String,
    pub on_flathub: bool,
    pub flathub_last_build: Option<chrono::DateTime<Utc>>,
    pub flathub_stats_installs: Option<f64>,
    pub flathub_stats_updates: Option<u64>,
    pub flathub_runtime: Option<String>,
    pub programming_languages: Vec<String>,
    pub gtk_version: Option<u32>,
    pub annotations: Vec<Annotation>,
    pub mobile_support: bool,
    pub keyword_collection: Vec<String>,
    pub flathub_verified: Option<bool>,
}

impl App {
    pub fn stable_releases(&self) -> Vec<Release> {
        self.releases
            .iter()
            .filter(|x| x.kind == "stable")
            .cloned()
            .collect()
    }
}

/// A minimalistic summary of an `App`
///
/// This representation is used for JavaScript access to a limit set of data.
#[derive(Debug, Serialize, Deserialize)]
pub struct AppMinimal {
    pub id: AppId,
    pub page_id: String,
    pub name: String,
    pub summary: String,
    pub programming_languages: Vec<String>,
}

impl From<&App> for AppMinimal {
    fn from(app: &App) -> Self {
        Self {
            id: app.id.clone(),
            page_id: app.page_id.clone(),
            name: app.name.clone(),
            summary: app.summary.clone(),
            programming_languages: app.programming_languages.clone(),
        }
    }
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct Apps(pub IndexMap<String, App>);

impl Apps {
    pub fn partial_translate(&mut self, translated_minimal: &AppsMinimal) {
        for app in translated_minimal.apps.values() {
            let untranslated = self.0.get_mut(&app.id.to_string()).unwrap();
            untranslated.name = app.name.clone();
            untranslated.summary = app.summary.clone();
        }
    }

    pub fn from_json_file<P: AsRef<Path>>(path: P) -> color_eyre::Result<Self> {
        let file = File::open(path)?;
        let reader = BufReader::new(file);

        let map = serde_json::from_reader(reader)?;

        Ok(Self(map))
    }

    pub fn from_upstream_cached() -> color_eyre::Result<Self> {
        let mut cache_path = download_cache_dir();
        std::fs::create_dir_all(&cache_path)?;
        cache_path.push("apps.json");

        match Self::from_json_file(&cache_path) {
            Ok(apps) => {
                info!("app data: Using cached file {cache_path:?}");
                Ok(apps)
            }
            Err(err) => {
                debug!("No cache available: {err}");
                info!("app data: Downloading from apps.gnome.org");
                let apps: Apps =
                    reqwest::blocking::get("https://apps.gnome.org/_api/apps.json")?.json()?;

                std::fs::write(&cache_path, serde_json::to_string(&apps)?)
                    .with_note(|| format!("Could not write to {cache_path:?}."))?;
                Ok(apps)
            }
        }
    }

    pub fn minimal(&self, lang: String) -> AppsMinimal {
        let apps = self
            .iter()
            .map(|(id, app)| (id.clone(), AppMinimal::from(app)))
            .collect();

        AppsMinimal { lang, apps }
    }

    pub fn shuffle(self) -> Self {
        let mut app_list: Vec<_> = self.0.into_values().collect();
        app_list.shuffle(&mut rand::thread_rng());
        Self::from(app_list)
    }
}

impl AsRef<IndexMap<String, App>> for Apps {
    fn as_ref(&self) -> &IndexMap<String, App> {
        &self.0
    }
}

impl std::ops::Deref for Apps {
    type Target = IndexMap<String, App>;

    fn deref(&self) -> &IndexMap<String, App> {
        self.as_ref()
    }
}

impl From<Vec<App>> for Apps {
    fn from(apps: Vec<App>) -> Self {
        let map: IndexMap<String, App> = apps.into_iter().map(|x| (x.id.to_string(), x)).collect();
        Self(map)
    }
}

#[derive(Debug, Serialize, Deserialize)]
pub struct AppsMinimal {
    lang: String,
    apps: IndexMap<String, AppMinimal>,
}

impl AppsMinimal {
    pub const FILENAME: &'static str = "apps-minimal.json";

    pub fn from_json_file<P: AsRef<Path>>(path: P) -> color_eyre::Result<Self> {
        let file = File::open(path)?;
        let reader = BufReader::new(file);

        let basic_apps = serde_json::from_reader(reader)?;

        Ok(basic_apps)
    }

    pub fn from_upstream_cached(lang: String) -> color_eyre::Result<Self> {
        let mut cache_path = download_cache_dir();
        cache_path.push(&lang);
        std::fs::create_dir_all(&cache_path)?;
        cache_path.push(Self::FILENAME);

        if let Ok(list) = Self::from_json_file(&cache_path) {
            info!("app data: Using cached file {cache_path:?}");
            Ok(list)
        } else {
            let url = format!("https://apps.gnome.org/_api/{lang}/{}", Self::FILENAME);
            info!("app data: Downloading from {url}");
            let apps = reqwest::blocking::get(&url).note(url)?.json()?;

            std::fs::write(&cache_path, serde_json::to_string(&apps)?)
                .with_note(|| format!("Could not write to {cache_path:?}."))?;
            Ok(apps)
        }
    }

    pub fn lang(&self) -> &str {
        self.lang.as_str()
    }

    pub fn set_lang(&mut self, lang: String) {
        self.lang = lang;
    }

    pub fn apps(&self) -> &IndexMap<String, AppMinimal> {
        &self.apps
    }
}
