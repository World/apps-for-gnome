use std::collections::BTreeSet;
use std::fmt::Display;
use std::fs::File;
use std::io::BufReader;
use std::ops::Deref;
use std::path::Path;

use color_eyre::Help;
use indexmap::IndexMap;
use rand::prelude::*;
use serde::{Deserialize, Serialize};

use super::ComponentId;
use crate::app;

#[derive(Default, Debug, Serialize, Deserialize, Clone, PartialEq, Eq, PartialOrd, Ord)]
pub struct AppId(String);

impl AppId {
    pub fn new(id: impl ToString) -> Self {
        Self(id.to_string())
    }
}

impl Display for AppId {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.write_str(&self.0)
    }
}

impl From<&str> for AppId {
    fn from(value: &str) -> Self {
        Self(value.to_string())
    }
}

impl Deref for AppId {
    type Target = str;
    fn deref(&self) -> &Self::Target {
        self.0.as_str()
    }
}

#[derive(Default, Debug, Serialize, Deserialize, Clone, Eq, PartialEq, PartialOrd, Ord)]
pub struct Person {
    pub name: Option<String>,
    pub pronouns: Option<String>,
    pub avatar_url: Option<String>,
    pub location: Option<String>,
    pub social_accounts: BTreeSet<SocialAccount>,
    pub websites: BTreeSet<String>,
    pub git_accounts: BTreeSet<GitAccount>,
    pub apps: BTreeSet<AppId>,
    pub components: BTreeSet<ComponentId>,
    pub teams: BTreeSet<TeamId>,
    pub gnome_account: Option<String>,
}

impl Person {
    pub fn id(&self) -> PersonId {
        if let Some(gnome_git_acc) = self
            .git_accounts
            .iter()
            .find(|x| x.url.contains("gitlab.gnome.org"))
        {
            PersonId(gnome_git_acc.username.clone())
        } else if let Some(acc) = self.git_accounts.first() {
            let url = reqwest::Url::parse(&acc.url).unwrap();
            let host = url.host().unwrap();
            PersonId(format!("{}-{host}", acc.username))
        } else if let Some(gnome_account) = &self.gnome_account {
            PersonId(format!("{gnome_account}-gnome"))
        } else {
            PersonId(String::from("unknown"))
        }
    }

    pub fn display_name(&self) -> String {
        if let Some(name) = &self.name {
            name.to_string()
        } else {
            self.id().to_string()
        }
    }
}

#[derive(Default, Debug, Serialize, Deserialize, Clone, Eq, PartialEq, PartialOrd, Ord, Hash)]
pub struct PersonId(pub String);

impl Display for PersonId {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.write_str(&self.0)
    }
}

#[derive(Default, Debug, Serialize, Deserialize, Clone)]
pub struct People {
    pub people: IndexMap<PersonId, Person>,
}

impl From<Vec<Person>> for People {
    fn from(people: Vec<Person>) -> Self {
        Self {
            people: IndexMap::from_iter(people.into_iter().map(|person| (person.id(), person))),
        }
    }
}

impl People {
    pub fn from_json_file<P: AsRef<Path>>(path: P) -> color_eyre::Result<Self> {
        let file = File::open(path)?;
        let reader = BufReader::new(file);

        let people = serde_json::from_reader(reader)?;

        Ok(people)
    }

    pub fn from_upstream_cached() -> color_eyre::Result<Self> {
        let mut cache_path = app::download_cache_dir();
        std::fs::create_dir_all(&cache_path)?;
        cache_path.push("people.json");

        match Self::from_json_file(&cache_path) {
            Ok(list) => {
                info!("people data: Using cached file {cache_path:?}");
                Ok(list)
            }
            Err(err) => {
                debug!("No cache available: {err}");
                info!("people data: Downloading from apps.gnome.org");
                let people =
                    reqwest::blocking::get("https://apps.gnome.org/_api/people.json")?.json()?;

                std::fs::write(&cache_path, serde_json::to_string(&people)?)
                    .with_note(|| format!("Could not write to {cache_path:?}."))?;
                Ok(people)
            }
        }
    }

    pub fn shuffle(self) -> Self {
        let mut list: Vec<_> = self.people.into_values().collect();
        list.shuffle(&mut rand::thread_rng());
        Self::from(list)
    }
}

impl AsRef<IndexMap<PersonId, Person>> for People {
    fn as_ref(&self) -> &IndexMap<PersonId, Person> {
        &self.people
    }
}

impl std::ops::Deref for People {
    type Target = IndexMap<PersonId, Person>;

    fn deref(&self) -> &Self::Target {
        self.as_ref()
    }
}

#[derive(Default, Debug, Serialize, Deserialize, Clone, Eq, PartialEq, PartialOrd, Ord, Hash)]
pub struct TeamId(pub String);

impl Display for TeamId {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.write_str(&self.0)
    }
}

#[derive(Debug, Serialize, Deserialize, Clone, Eq, PartialEq, PartialOrd, Ord)]
pub struct SocialAccount {
    pub provider: String,
    pub url: String,
    pub display: String,
}

#[derive(Debug, Serialize, Deserialize, Clone, Eq, PartialEq, PartialOrd, Ord)]
pub struct GitAccount {
    pub url: String,
    pub username: String,
}

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct Release {
    pub date: Option<chrono::DateTime<chrono::Utc>>,
    pub locale_date: Option<String>,
    pub version: String,
    pub description: String,
    pub kind: String,
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct Screenshot {
    pub default: bool,
    pub file: String,
    pub caption: String,
    pub medium: String,
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct TranslationStats {
    pub percentage: Option<u32>,
    pub locale: String,
}

#[derive(Debug, Clone, Serialize, Deserialize, PartialEq, Eq, PartialOrd, Ord)]
pub struct Annotation {
    pub level: String,
    pub msg: String,
}

#[derive(Debug, Clone, Copy, Serialize, Deserialize, PartialEq, Eq, PartialOrd, Ord)]
#[serde(rename_all = "kebab-case")]
pub enum GnomeCategory {
    Core,
    Development,
    Incubator,
    Circle,
}

impl std::fmt::Display for GnomeCategory {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let s = match self {
            Self::Core => "core",
            Self::Development => "development",
            Self::Circle => "circle",
            Self::Incubator => "incubator",
        };

        f.write_str(s)
    }
}
