use std::fmt::Display;
use std::fs::File;
use std::io::BufReader;
use std::path::Path;

use color_eyre::Help;
use indexmap::IndexMap;
use serde::{Deserialize, Serialize};

use super::{app, PersonId};
use crate::GnomeCategory;

#[derive(Debug, Serialize, Deserialize, Clone, Eq, PartialEq, PartialOrd, Ord)]
pub struct Component {
    pub id: ComponentId,
    pub gnome_category: GnomeCategory,
    pub repo_url: String,
    pub name: Option<String>,
    pub avatar_url: Option<String>,
    pub shortdesc: Option<String>,
    pub description: Option<String>,
    pub homepage: Option<String>,
    pub topics: Vec<String>,
    pub maintainers: Vec<PersonId>,
}

impl Component {
    pub fn id(&self) -> ComponentId {
        self.id.clone()
    }
}

#[derive(Default, Debug, Serialize, Deserialize, Clone, Eq, PartialEq, PartialOrd, Ord, Hash)]
pub struct ComponentId(pub String);

impl Display for ComponentId {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.write_str(&self.0)
    }
}

#[derive(Default, Debug, Serialize, Deserialize, Clone)]
pub struct Components {
    pub components: IndexMap<ComponentId, Component>,
}

impl From<Vec<Component>> for Components {
    fn from(mut components: Vec<Component>) -> Self {
        components.sort_by_key(|x| x.id.clone());
        Self {
            components: IndexMap::from_iter(components.into_iter().map(|c| (c.id(), c))),
        }
    }
}

impl Components {
    pub fn from_json_file<P: AsRef<Path>>(path: P) -> color_eyre::Result<Self> {
        let file = File::open(path)?;
        let reader = BufReader::new(file);

        let components = serde_json::from_reader(reader)?;

        Ok(components)
    }

    pub fn from_upstream_cached() -> color_eyre::Result<Self> {
        let mut cache_path = app::download_cache_dir();
        std::fs::create_dir_all(&cache_path)?;
        cache_path.push("components.json");

        match Self::from_json_file(&cache_path) {
            Ok(list) => {
                info!("components data: Using cached file {cache_path:?}");
                Ok(list)
            }
            Err(err) => {
                debug!("No cache available: {err}");
                info!("components data: Downloading from apps.gnome.org");
                let components =
                    reqwest::blocking::get("https://apps.gnome.org/_api/components.json")?
                        .json()?;

                std::fs::write(&cache_path, serde_json::to_string(&components)?)
                    .with_note(|| format!("Could not write to {cache_path:?}."))?;
                Ok(components)
            }
        }
    }
}
