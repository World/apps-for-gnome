pub mod templates;

use std::collections::hash_map::DefaultHasher;
use std::hash::{Hash, Hasher};
use std::io::prelude::*;
use std::path::{Path, PathBuf};
use std::sync::{LazyLock, Mutex};

use chrono::Datelike;
use icu_calendar::{DateTime, Iso};
use icu_collator::{Collator, CollatorOptions};
use icu_datagen::blob_exporter::BlobExporter;
use icu_datagen::{DatagenDriver, DatagenProvider};
use icu_datetime::options::length;
use icu_datetime::{DateFormatter, DateTimeFormatter, DateTimeFormatterOptions};
use icu_experimental::displaynames::{LanguageDisplayNames, LocaleDisplayNamesFormatter};
use icu_provider::DataKey;
use icu_provider_blob::BlobDataProvider;
use indexmap::{IndexMap, IndexSet};
use itertools::Itertools;
use serde::{Deserialize, Serialize};

type StringMap = LazyLock<Mutex<IndexMap<String, StringInfo>>>;

pub static TRANSLATABLE_STRINGS: StringMap = LazyLock::new(|| Mutex::new(Default::default()));
pub static ICU_DATA_PROVIDER: LazyLock<BlobDataProvider> = LazyLock::new(load_icu_data);

#[derive(Default, Clone, Debug)]
pub struct StringInfo {
    locations: IndexSet<StringLocation>,
    comments: IndexSet<String>,
}

#[derive(Debug, Clone, Hash, Eq, PartialEq, Ord, PartialOrd)]
pub struct StringLocation {
    path: PathBuf,
    line: Option<u32>,
}

impl StringLocation {
    pub fn new(path: impl AsRef<Path>) -> Self {
        let mut new_path = PathBuf::from("templates");
        new_path.push(path);

        Self {
            path: new_path,
            line: None,
        }
    }

    pub fn line(mut self, line: u32) -> Self {
        self.line = Some(line);
        self
    }
}

pub fn translate(s: &str) -> String {
    if std::env::var("MARK_TRANSLATED").is_ok() {
        format!("🅰️{}🅱️", gettextrs::gettext(s))
    } else {
        gettextrs::gettext(s)
    }
}

pub fn add_string(s: &str, location: Option<StringLocation>) {
    let mut strings = TRANSLATABLE_STRINGS.lock().unwrap();
    let string_info = strings.entry(s.to_string()).or_default();

    if let Some(location) = location {
        string_info.locations.insert(location);
    }
}

pub fn add_string_with_comment(s: &str, comment: &str) {
    TRANSLATABLE_STRINGS
        .lock()
        .unwrap()
        .entry(s.to_string())
        .or_default()
        .comments
        .insert(comment.to_string());
}

pub fn write_pot_file(path: impl AsRef<Path>) -> std::io::Result<()> {
    const POT_HEADER: &str = r#"msgid ""
msgstr ""
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

"#;

    let mut pot = String::from(POT_HEADER);

    let mut msgids = TRANSLATABLE_STRINGS.lock().unwrap().clone();
    // Sort by location first and by string afterwards
    msgids.sort_by(|k1, v1, k2, v2| {
        v1.locations
            .first()
            .cmp(&v2.locations.first())
            .then_with(|| k1.cmp(k2))
    });

    for (msgid, details) in msgids {
        // Make valid C literal
        let msgid_escaped = msgid.replace('\\', r"\\").replace('\n', r"\n");

        let locations_by_path = details.locations.iter().chunk_by(|x| &x.path);
        // Limit number of lines linsted per file
        let locations = locations_by_path.into_iter().flat_map(|(_, x)| x.take(3));

        for detail in locations {
            pot.push_str(&format!(r"#: {}", detail.path.display()));
            if let Some(line) = detail.line {
                pot.push_str(&format!(r":{line}"));
            }
            pot.push('\n');
        }

        for comment in &details.comments {
            pot.push_str(&format!("#. Translators: {}\n", comment));
        }

        pot.push_str(&format!(r#"msgid "{msgid_escaped}""#));
        pot.push('\n');
        pot.push_str(r#"msgstr """#);
        pot.push_str("\n\n");
    }

    std::fs::write(path, &pot)
}

pub fn init(domain: &str) {
    // We have to set this to something to keep gettext happy
    std::env::set_var("LANG", "en_US.utf-8");
    gettextrs::setlocale(gettextrs::LocaleCategory::LcAll, "");

    gettextrs::bindtextdomain(domain, "install/share/locale").unwrap();
    gettextrs::textdomain(domain).unwrap();
    gettextrs::bind_textdomain_codeset(domain, "UTF-8").unwrap();
}

pub fn set_lang(lang: &str) {
    std::env::set_var("LANGUAGE", lang);
    gettextrs::setlocale(gettextrs::LocaleCategory::LcAll, "");
}

fn language_tags_ietf() -> Vec<String> {
    let mut languages = vec![String::from("en")];

    let file = std::fs::File::open("po/LINGUAS").unwrap();
    for line in std::io::BufReader::new(file).lines() {
        languages.push(line.unwrap().replace('_', "-"));
    }

    languages.sort();

    languages
}

pub fn datetime(datetime: &chrono::DateTime<chrono::Utc>, lang: &Language) -> String {
    let lang = &lang.tag_ietf;

    let datetime = chrono_to_icu(datetime);
    let locale = lang.parse::<icu_locid::Locale>().unwrap();

    DateTimeFormatter::try_new_with_buffer_provider(
        &*ICU_DATA_PROVIDER,
        &locale.into(),
        DateTimeFormatterOptions::Length(length::Bag::from_date_time_style(
            length::Date::Medium,
            length::Time::Short,
        )),
    )
    .unwrap()
    .format_to_string(&datetime.to_any())
    .unwrap()
}

pub fn date(datetime: &chrono::DateTime<chrono::Utc>, lang: &str) -> String {
    let datetime = chrono_to_icu(datetime);
    let locale = lang.parse::<icu_locid::Locale>().unwrap();

    DateFormatter::try_new_with_length_with_buffer_provider(
        &*ICU_DATA_PROVIDER,
        &locale.into(),
        length::Date::Medium,
    )
    .unwrap()
    .format_to_string(&datetime.to_any())
    .unwrap()
}

use chrono::prelude::*;

pub fn chrono_to_icu(datetime: &chrono::DateTime<chrono::Utc>) -> DateTime<Iso> {
    DateTime::try_new_iso_datetime(
        datetime.year(),
        datetime.month() as u8,
        datetime.day() as u8,
        datetime.hour() as u8,
        datetime.minute() as u8,
        datetime.second() as u8,
    )
    .unwrap()
}

pub fn languages() -> Vec<Language> {
    language_tags_ietf()
        .into_iter()
        .map(Language::from_ietf_tag)
        .collect()
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct Language {
    pub tag_unix: String,
    pub tag_ietf: String,
    pub local_name: String,
}

pub fn collator(lang: &str) -> Collator {
    let lang = Language::from_ietf_tag(lang.to_string()).tag_ietf;

    let locale = lang.parse::<icu_locid::Locale>().unwrap();
    let options = CollatorOptions::new();

    Collator::try_new_with_buffer_provider(&*ICU_DATA_PROVIDER, &locale.clone().into(), options)
        .unwrap()
}

impl Language {
    pub fn from_ietf_tag(tag_ietf: impl ToString) -> Self {
        let mut tag_ietf = tag_ietf.to_string().replace('_', "-");
        if tag_ietf.is_empty() {
            tag_ietf.push_str("en");
        }

        let local_name = display_name(&tag_ietf);
        let tag_unix = tag_ietf.replace('-', "_");

        Self {
            tag_unix,
            tag_ietf,
            local_name,
        }
    }

    pub fn en() -> Self {
        Self {
            local_name: "English".into(),
            tag_ietf: "en".into(),
            tag_unix: "en".into(),
        }
    }
}

fn display_name(tag: &str) -> String {
    let locale = tag.parse::<icu_locid::Locale>().unwrap();

    if let Ok(display_name) = LocaleDisplayNamesFormatter::try_new_with_buffer_provider(
        &*ICU_DATA_PROVIDER,
        &locale.clone().into(),
        Default::default(),
    ) {
        return display_name.of(&locale).to_string();
    } else if let Ok(display_name) = LanguageDisplayNames::try_new_with_buffer_provider(
        &*ICU_DATA_PROVIDER,
        &locale.clone().into(),
        Default::default(),
    ) {
        if let Ok(language) = tag.parse::<icu_locid::subtags::Language>() {
            if let Some(name) = display_name.of(language) {
                return name.to_string();
            }
        }
    }

    // Fallback to Engish
    if let Ok(display_name) = LocaleDisplayNamesFormatter::try_new_with_buffer_provider(
        &*ICU_DATA_PROVIDER,
        &icu_locid::locale!("en").into(),
        Default::default(),
    ) {
        return display_name.of(&locale).to_string();
    }

    String::from(tag)
}

fn icu_keys() -> Vec<DataKey> {
    icu_datagen::keys(&[
        "collator/data@1",
        "collator/dia@1",
        "collator/jamo@1",
        "collator/meta@1",
        "collator/reord@1",
        "datetime/gregory/datelengths@1",
        "datetime/gregory/datesymbols@1",
        "datetime/timelengths@1",
        "datetime/timesymbols@1",
        "decimal/symbols@1",
        "displaynames/languages@1",
        "displaynames/locales@1",
        "displaynames/regions@1",
        "displaynames/scripts@1",
        "displaynames/variants@1",
        "normalizer/nfd@1",
        "normalizer/nfdex@1",
    ])
}

fn load_icu_data() -> BlobDataProvider {
    let languages = language_tags_ietf();
    let keys = icu_keys();

    // Created filename from hash to ensure data are loaded again for new languages
    // etc
    let mut hash = DefaultHasher::new();
    languages.hash(&mut hash);
    keys.hash(&mut hash);

    let filename = format!("icu-{:x}.postcard", hash.finish());
    let mut path = crate::download_cache_dir();
    path.push(filename);

    if !path.is_file() {
        let language_identifiers: Vec<_> = languages
            .into_iter()
            .map(|x| x.parse::<icu_locid::LanguageIdentifier>().unwrap())
            .collect();

        let file = std::fs::File::create(&path).unwrap();

        DatagenDriver::new()
            .with_keys(keys)
            .with_locales(language_identifiers)
            .export(
                &DatagenProvider::new_latest_tested(),
                BlobExporter::new_with_sink(Box::new(file)),
            )
            .unwrap();
    }

    let blob = std::fs::read(&path).unwrap();

    BlobDataProvider::try_new_from_blob(blob.into_boxed_slice()).unwrap()
}
