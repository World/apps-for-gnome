const supportedLanguages =
    {{ languages|json }}
    ;

function redirectToUserLang(baseUrl, langBaseUrl) {
  const storedLang = localStorage.getItem('gnome_configured_language');

  const newLang =
    ([storedLang] ?? [])
    .concat(navigator.languages)
    .concat(navigator.languages.map(lang => lang.split('-')[0]))
    .filter(lang => supportedLanguages.map((x) => x.tag_ietf).includes(lang))
    .shift();

    if (newLang && newLang != document.documentElement.lang) {
      const url = newLangURL(baseUrl, langBaseUrl, newLang);
      window.location.replace(url);
    }
}

function newLangURL(baseUrl, langBaseUrl, lang) {
  let path = window.location.pathname;

  if (path.startsWith(langBaseUrl)) {
    path = path.slice(langBaseUrl.length);
  } else if (path.startsWith(baseUrl)) {
    path = path.slice(baseUrl.length);
  }
  const targetPathname = `${baseUrl}/${lang}${path}`;

  let url = new URL(window.location.href);
  url.pathname = targetPathname;

  return url;
}

function fillLanguageList(ul, baseUrl, langBaseUrl) {
  let newChildren = [];

  supportedLanguages.forEach((lang) => {
    const url = newLangURL(baseUrl, langBaseUrl, lang.tag_ietf);

    const li = document.createElement('li');
    const a = document.createElement('a');
    a.innerText = lang.local_name;
    a.setAttribute('title', lang.tag_ietf);
    a.setAttribute('href', url);
    a.addEventListener('click', (event) => { event.preventDefault(); setLanguage(lang.tag_ietf, url); });

    li.appendChild(a);
    newChildren.push(li);
  });

  ul.replaceChildren(...newChildren);
}

function setLanguage(lang, url) {
  localStorage.setItem('gnome_configured_language', lang);
  window.location.replace(url);
}