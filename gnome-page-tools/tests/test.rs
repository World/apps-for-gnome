use gnome_page_tools::markdown::md_to_html;

#[test]
fn test() {
    std::env::set_var("MARK_TRANSLATED", "1");
    let result = md_to_html(
        &std::fs::read_to_string("tests/simple.md").unwrap(),
        "simple.md",
    )
    .unwrap();

    assert_eq!(
        result,
        std::fs::read_to_string("tests/simple.html").unwrap()
    );
}
