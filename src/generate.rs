mod card_icons;
mod pages;
mod redirects;
mod screenshots;
mod sitemap;

use std::sync::Arc;

use clap::Parser;
use color_eyre::eyre::{bail, WrapErr};
use flatpak::prelude::*;
use libappstream::prelude::*;
use page_tools::output::{Dir, File, Output, OutputSimple, Scss};
use page_tools::{AppId, Language};
use flatpak::gio;

use crate::collect::*;
use crate::context::*;
use crate::output::json;
use crate::{cli, flathub, lists, log_warn, output, timed_info};

pub async fn generate() -> color_eyre::Result<()> {
    page_tools::i18n::init("apps-gnome-org");
    page_tools::output::prepare();

    let args = Arc::new(cli::Args::parse());

    let mut app_ids = Vec::new();

    if !args.dev {
        app_ids.append(&mut lists::gnome_app_ids(GnomeProject::CoreApps).await?);
        app_ids.append(&mut lists::gnome_app_ids(GnomeProject::Circle).await?);
    }

    app_ids.append(&mut lists::gnome_app_ids(GnomeProject::CoreDeveloperTools).await?);
    app_ids.append(&mut lists::gnome_app_ids(GnomeProject::Incubator).await?);

    if let Some(app) = &args.app_id {
        app_ids.retain(|x| &x.app_id.to_string() == app);
    }

    timed_info!("Loading appdata file");
    let installation = flathub::installation()?;

    installation.update_appstream_sync("afg-flathub", None, gio::Cancellable::NONE)?;

    let remote = installation.remote_by_name("afg-flathub", gio::Cancellable::NONE)?;
    let appstream_path = remote.appstream_dir(None).unwrap().child("appstream.xml");

    let pool = libappstream_pool(&appstream_path.path().unwrap())?;
    pool.load(gio::Cancellable::NONE)?;

    timed_info!("Installing Flathub data");
    let flathub_app_ids = app_ids
        .iter()
        .map(|x| &x.app_id)
        .filter(|x| {
            !pool.components_by_id(x).unwrap().is_empty()
                || !pool
                    .components_by_id(&format!("{x}.desktop"))
                    .unwrap()
                    .is_empty()
        })
        .collect::<Vec<&AppId>>();

    flathub::install_data(&flathub_app_ids)?;

    timed_info!("Adding appstream components not available on Flathub");
    if let Ok(path) = std::fs::canonicalize("non-flatpak/metainfo") {
        pool.add_extra_data_location(&path.to_string_lossy(), libappstream::FormatStyle::Catalog);
        pool.load(gio::Cancellable::NONE)?;
    } else {
        error!("Non-flatpak data not available. Use `./scripts/download-non-flatpaked.py` to get them.");
        panic!();
    }

    timed_info!("Copying app icons");
    for dir in ["public.new/icons/scalable", "public.new/icons/symbolic"] {
        std::fs::create_dir_all(dir).wrap_err_with(|| format!("Could not create {dir}."))?;
    }
    for x in &app_ids {
        let app_id = &x.app_id;
        for (kind, suffix) in [("scalable", ""), ("symbolic", "-symbolic")] {
            // svg
            let mut dest = std::path::PathBuf::from("public.new/icons");
            dest.push(kind);
            dest.push(format!("{app_id}.svg"));

            let mut src = std::path::PathBuf::from("flathub-installation/app/");
            src.push(app_id.to_string());
            src.push(flatpak::functions::default_arch().unwrap());
            src.push("stable/active/files/share/icons/hicolor/");
            src.push(kind);
            src.push("apps/");
            src.push(format!("{app_id}{suffix}.svg"));
            src.push(format!("{app_id}{suffix}.svg"));
            if src.exists() {
                std::fs::copy(src, dest)?;
            } else {
                debug!("No {} app icon at {:?}", kind, src);
                let mut src = std::path::PathBuf::from("non-flatpak/icons");
                src.push(format!("{app_id}{suffix}.svg"));
                if src.exists() {
                    std::fs::copy(src, dest)?;
                } else {
                    debug!("No {kind} app icon at {src:?}");
                    log_warn!(app_id.to_string(), "No {kind} app icon");
                }
            }
        }
    }

    timed_info!("Retrieving nightly infos");
    installation.update_appstream_sync("afg-gnome-nightly", None, gio::Cancellable::NONE)?;
    let nightly_app_ids = Arc::new(
        installation
            .list_remote_refs_sync("afg-gnome-nightly", gio::Cancellable::NONE)?
            .into_iter()
            .filter(|x| x.kind() == flatpak::RefKind::App)
            .filter_map(|x| x.name())
            .map(|x| x.to_string())
            .collect::<Vec<_>>(),
    );

    let mut stats = flathub_stats::Stats::default();
    if !args.skip_stats {
        timed_info!("Download stats");
        let date: chrono::NaiveDate =
            chrono::Utc::now().date_naive() - chrono::Duration::days(12 * 30);
        stats = flathub_stats::Stats::download(date).await;
    }
    let stats = Arc::new(stats);

    timed_info!("Extracting and retrieving relevant data");
    let mut tasks = tokio::task::JoinSet::new();
    for x in app_ids {
        tasks.spawn(AppRaw::new(
            x.app_id,
            x.category,
            pool.clone(),
            args.clone(),
            stats.clone(),
            nightly_app_ids.clone(),
            x.circle_details.clone(),
        ));
    }

    let mut apps = Vec::new();
    while let Some(app) = tasks.join_next().await {
        if let Some(app) = app? {
            apps.push(app);
        } else {
            bail!("Aborting because an app is missing.")
        }
    }

    timed_info!("Preparing languages");
    let mut languages = page_tools::i18n::languages();

    if args.dev {
        languages.retain(|lang| ["en", "de", "he", "en_GB"].contains(&lang.tag_unix.as_str()));
    }

    if !args.skip_pages {
        timed_info!("Generating pages");
        for lang in languages.iter() {
            if lang.tag_ietf == "en" {
                pool.set_locale("");
            } else {
                pool.set_locale(&lang.tag_ietf);
            }
            pool.load(gio::Cancellable::NONE).unwrap();

            let apps = page_tools::Apps::from(apps.out(&lang.tag_unix));
            pages::generate(&apps, &lang.tag_unix);
            json::generate_minimal(apps.minimal(lang.tag_unix.to_string())).await?;
        }
    }

    // Set back to en for API
    pool.set_locale("");
    pool.load(gio::Cancellable::NONE).unwrap();

    if !args.skip_components {
        timed_info!("Getting component infos");
        let components = components::collect(args.clone(), &apps).await;
        timed_info!("Generating components.json API");
        json::generate_components(&components).await?;
    }

    timed_info!("Generate language js");
    let languages_js = page_tools::i18n::templates::LanguagesJs::new(languages.clone());
    languages_js.output("assets/js/languages.js");

    timed_info!("Generating 404 error page ...");
    page_tools::i18n::set_lang("en");
    crate::pages::Error404 {
        site: &page_tools::Site::new(&Language::from_ietf_tag("en")),
    }
    .output("404.html");

    timed_info!("Copying assets");
    Dir::from("assets/").output("assets/");
    File::from("data/google7ed0380f213cfb6e.html").output("google7ed0380f213cfb6e.html");
    File::from("data/robots.txt").output("robots.txt");

    if !args.skip_images {
        timed_info!("Generating card icons");
        card_icons::generate(&apps).await.unwrap();

        timed_info!("Copying screenshots");
        screenshots::generate(&apps).await;
    }

    timed_info!("Compiling sass");
    Scss::from("scss/style.scss").output("assets/style.css");

    timed_info!("Generating sitemap");
    sitemap::generate(&apps, &languages);

    timed_info!("Generating redirects");
    redirects::generate(&languages)?;

    timed_info!("Generating apps.json API");
    output::json::generate_apps(&apps).await?;

    timed_info!("Generating official-gnome-apps.json API");
    output::json::generate_official_gnome_apps(&apps).await?;

    timed_info!("Generating people.json API");
    output::json::generate_people().await?;

    timed_info!("Moving finalized data");
    page_tools::output::complete().unwrap();

    if args.regen_pot {
        page_tools::i18n::write_pot_file("po/apps.gnome.org.pot").unwrap();
    }

    timed_info!("Page generation completed");
    Ok(())
}

fn libappstream_pool(path: &std::path::Path) -> color_eyre::Result<libappstream::Pool> {
    let pool = libappstream::Pool::new();
    pool.set_flags(libappstream::PoolFlags::NONE);
    pool.add_extra_data_location(
        &path.parent().unwrap().to_string_lossy(),
        libappstream::FormatStyle::Catalog,
    );
    Ok(pool)
}
