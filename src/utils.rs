use std::ops::Deref;

use chrono::{DateTime, Utc};
use color_eyre::eyre::Context;
use libappstream::prelude::*;

pub fn page_id(app_id: impl ToString) -> String {
    let s = app_id.to_string();
    let id = s.split('.').last().unwrap_or(&s);
    let mut words: Vec<String> = id
        .split(|x| x == '-' || x == '_')
        .map(|x| x.to_owned())
        .collect();

    for word in &mut words {
        word.get_mut(0..1).unwrap().make_ascii_uppercase();
    }

    words.join("")
}

pub fn escape_markup(text: &str) -> String {
    let escpaped = text.replace('<', "&lt;").replace('>', "&gt;");

    regex_replace_all!(
        r#"&lt;(/?)(p|ol|ul|li|em|code)&gt;"#i,
        &escpaped,
        |_, closing, tag| format!("<{}{}>", closing, tag)
    )
    .to_string()
}

pub fn uppercase_first_letter(s: &str) -> String {
    let mut c = s.chars();
    match c.next() {
        None => String::new(),
        Some(f) => f.to_uppercase().collect::<String>() + c.as_str(),
    }
}

/// Converts `[(145, 65, 172), ...]` format
pub fn parse_gs_color(colors: &str) -> Option<Vec<palette::Srgb>> {
    let colors = colors.replace(&['[', ']', ' '][..], "");

    let mut vec: Vec<(u8, u8, u8)> = Vec::new();

    for color in colors.split("),") {
        let rgb: Option<(&str, &str, &str, &str)> =
            regex_captures!(r"\(([0-9]+),([0-9]+),([0-9]+)\)?", color);
        if let Some((_, r, g, b)) = rgb {
            vec.push((r.parse().ok()?, g.parse().ok()?, b.parse().ok()?));
        } else {
            error!("Cannot parse color: {:?}", color);
            return None;
        }
    }

    Some(
        vec.into_iter()
            .map(|(r, g, b)| palette::Srgb::new(r as f32 / 255., g as f32 / 255., b as f32 / 255.))
            .collect(),
    )
}

pub fn to_css_color(color: &palette::Srgb) -> String {
    let css_color: palette::rgb::PackedRgba = color.into_format().into();

    format!("#{:08X}", css_color.color)[0..7].to_string()
}

pub async fn new_file_tokio<T: AsRef<std::path::Path>>(
    path: T,
) -> color_eyre::Result<tokio::fs::File> {
    let mut full_path = std::path::PathBuf::from("public.new");
    full_path.push(&path);

    if let Some(parent) = full_path.parent() {
        std::fs::create_dir_all(parent)
            .wrap_err_with(|| format!("Could not create {parent:#?}."))?;
    }

    let file = tokio::fs::File::create(&full_path)
        .await
        .wrap_err_with(|| format!("Could not create {full_path:#?}"))?;

    Ok(file)
}

pub fn libappstream_datetime(timestamp: u64) -> Option<DateTime<Utc>> {
    if timestamp == 0 {
        None
    } else {
        Some(DateTime::from_timestamp(timestamp as i64, 0).unwrap())
    }
}

pub fn libappstream_component(
    pool: &libappstream::Pool,
    app_id: &str,
) -> Option<UnsafeSend<libappstream::Component>> {
    pool.components_by_id(app_id)
        .and_then(|x| x.index_safe(0).map(UnsafeSend))
        .or_else(|| {
            pool.components_by_id(&format!("{app_id}.desktop"))
                .and_then(|x| x.index_safe(0).map(UnsafeSend))
        })
}

#[derive(Clone, Debug)]
pub struct UnsafeSend<T>(pub T);

unsafe impl<T> Send for UnsafeSend<T> {}
unsafe impl<T> Sync for UnsafeSend<T> {}

impl<T> Deref for UnsafeSend<T> {
    type Target = T;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
