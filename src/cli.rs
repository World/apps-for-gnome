use clap::Parser;

/// Apps for GNOME
#[derive(Parser, Debug)]
pub struct Args {
    /// Faster runs by generating fewer apps without localization
    #[arg(short, long)]
    pub dev: bool,
    /// Only process the specified app
    #[arg(short, long)]
    pub app_id: Option<String>,
    /// Pass the GitHub token, necessary for certain tasks
    #[arg(long)]
    pub github_token: Option<String>,
    /// Pass the GitLab token, necessary for certain tasks
    #[arg(long)]
    pub gitlab_token: Option<Vec<GitLabToken>>,
    /// Don't generate images
    #[arg(long)]
    pub skip_images: bool,
    /// Don't download stats
    #[arg(long)]
    pub skip_stats: bool,
    /// Don't generate app pages
    #[arg(long)]
    pub skip_pages: bool,
    /// Don't generate component api
    #[arg(long)]
    pub skip_components: bool,
    /// Regenerate .pot-files (translation strings)
    #[arg(long)]
    pub regen_pot: bool,
}

#[derive(Debug, Clone)]
pub struct GitLabToken {
    pub host: String,
    pub token: String,
}

impl std::str::FromStr for GitLabToken {
    type Err = String;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let mut split = s.splitn(2, '=');

        Ok(Self {
            host: split.next().ok_or("Format <host>=<token>")?.to_string(),
            token: split.next().ok_or("Format <host>=<token>")?.to_string(),
        })
    }
}
