use heck::ToUpperCamelCase;
use page_tools::AppId;
use serde::{Deserialize, Serialize};

use super::collect::*;
use super::context::*;
use crate::{config, timed_info};

pub async fn ldap_to_gitlab() -> &'static std::collections::HashMap<String, String> {
    pub static LDAP_TO_GITLAB: tokio::sync::OnceCell<std::collections::HashMap<String, String>> =
        tokio::sync::OnceCell::const_new();

    LDAP_TO_GITLAB
        .get_or_init(|| async {
            let doc =
                reqwest::get("https://maintainers-json-gitlab-cronjobs.apps.openshift.gnome.org/")
                    .await
                    .unwrap()
                    .json()
                    .await
                    .unwrap();
            super::gnomeid::gitlab_map(&doc)
        })
        .await
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct CircleDetails {
    pub app_id: AppId,
    pub added: String,
}

async fn circle_apps() -> color_eyre::Result<Vec<AppFundamental>> {
    let url = "https://gitlab.gnome.org/Teams/Circle/-/raw/main/data/apps.json";

    let file_u8 = reqwest::get(url).await?.bytes().await?;
    let app_ids: Vec<CircleDetails> = serde_json::from_slice(&file_u8)?;

    Ok(app_ids
        .into_iter()
        .map(|x| AppFundamental {
            app_id: x.app_id.clone(),
            category: GnomeProject::Circle,
            circle_details: Some(x),
        })
        .collect())
}

pub struct AppFundamental {
    pub app_id: AppId,
    pub category: GnomeProject,
    pub circle_details: Option<CircleDetails>,
}

impl AppFundamental {
    pub fn new(app_id: AppId, category: GnomeProject) -> Self {
        Self {
            app_id,
            category,
            circle_details: None,
        }
    }
}

pub async fn gnome_app_ids(category: GnomeProject) -> color_eyre::Result<Vec<AppFundamental>> {
    timed_info!("Loading list of app ids for {category:?}");

    if matches!(category, GnomeProject::Circle) {
        circle_apps().await
    } else {
        let (mut app_ids, additional_ids) = match category {
            GnomeProject::CoreApps => (
                gnome_build_meta_app_ids(category).await?,
                config::ADDITIONAL_CORE_APPS,
            ),

            GnomeProject::CoreDeveloperTools => (
                gnome_build_meta_app_ids(category).await?,
                config::ADDITIONAL_DEVELOPMENT_TOOLS,
            ),
            GnomeProject::Circle => unreachable!(),
            GnomeProject::Incubator => (Vec::new(), config::ADDITION_INCUBATOR_APPS),
        };

        app_ids.append(&mut additional_ids.iter().map(|x| AppId::from(*x)).collect());

        Ok(app_ids
            .into_iter()
            .map(|x| AppFundamental::new(x, category))
            .collect())
    }
}

async fn gnome_build_meta_app_ids(category: GnomeProject) -> color_eyre::Result<Vec<AppId>> {
    let url = format!(
        "https://gitlab.gnome.org/GNOME/gnome-build-meta/-/raw/{}/elements/core/meta-gnome-core-{}.bst",
         crate::config::GNOME_RELEASE,
         category.as_list_name());

    let file_u8 = reqwest::get(url).await?.bytes().await?;
    let names: Bst = serde_yaml::from_slice(&file_u8)?;

    let mut app_ids = Vec::new();
    for name in names.depends {
        let mut id = name;

        let split: Vec<&str> = id.split('/').collect();

        if let Some(part_two) = split.get(1) {
            id = (*part_two).to_string();
        }

        if let Some(rest) = id.strip_prefix("gnome-") {
            id = rest.to_string();
        }

        if let Some(rest) = id.strip_suffix(".bst") {
            id = rest.to_string();
        }

        id = format!("org.gnome.{}", id.to_upper_camel_case());

        app_ids.push(real_id(id));
    }

    Ok(app_ids)
}

#[derive(Debug, Serialize, Deserialize)]
struct Bst {
    depends: Vec<String>,
}
