use std::collections::{BTreeMap, BTreeSet};
use std::fs::File;
use std::path::PathBuf;
use std::sync::Arc;

use color_eyre::eyre::Context;
use page_tools::{Component, ComponentId, Components, GnomeCategory, TeamId};
use serde::{Deserialize, Serialize};
use tokio::task::JoinSet;

use super::git_users::IntoProjectId;
use super::AppRaw;
use crate::cli::Args;
use crate::collect::git_repos::{self, Repo};
use crate::collect::git_users;

const GNOME_BUILD_META_DIR: &str = "cache/gnome-build-meta/elements";
const CORE_FOLDERS: &[&str] = &["core", "core-deps", "sdk", "sdk-deps"];

const CIRCLE_URL: &str =
    "https://gitlab.gnome.org/Teams/Circle/-/raw/wip/sophie-h/components/data/components.yml";

#[derive(Debug, Clone, Serialize, Deserialize)]
struct BstElement {
    sources: Option<Vec<Source>>,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
struct Source {
    url: Option<String>,
}

pub async fn collect(args: Arc<Args>, apps: &[AppRaw]) -> Components {
    let app_repos = apps
        .iter()
        .filter_map(|x| x.repo.clone())
        .collect::<BTreeSet<_>>();

    let all_core_components: Vec<(ComponentId, Repo)> = core_components().await;
    // Filter out apps
    let core_components = all_core_components
        .into_iter()
        .filter(|(_, x)| !app_repos.contains(x))
        .collect::<Vec<_>>();
    let mut component_infos =
        project_infos(core_components, args.clone(), GnomeCategory::Core).await;

    let circle_components = circle_components().await;
    component_infos
        .append(&mut project_infos(circle_components, args.clone(), GnomeCategory::Circle).await);

    let teams = teams();
    project_infos(teams, args.clone(), GnomeCategory::Core).await;

    Components::from(
        component_infos
            .into_iter()
            .map(|(info, id, repo, gnome_category)| component(info, id, repo, gnome_category))
            .collect::<Vec<_>>(),
    )
}

pub fn component(
    project_info: git_repos::ProjectInfo,
    id: ComponentId,
    repo: Repo,
    gnome_category: GnomeCategory,
) -> Component {
    let repo_url = project_info
        .doap
        .repository
        .repositories
        .first()
        .and_then(|x| x.browse.as_ref())
        .map(|x| x.resource.clone())
        .unwrap_or_else(|| repo.url());

    Component {
        id,
        gnome_category,
        avatar_url: project_info.repo_info.avatar_url(),
        repo_url,
        name: project_info.doap.name,
        shortdesc: project_info.doap.shortdesc,
        description: project_info.doap.description,
        homepage: project_info.doap.homepage.map(|x| x.resource),
        topics: project_info.repo_info.topics,
        maintainers: project_info
            .maintainers
            .into_iter()
            .map(|x| x.read().unwrap().id())
            .collect(),
    }
}

pub fn teams() -> Vec<(TeamId, Repo)> {
    vec![
        (
            TeamId("Circle Committee".into()),
            Repo::for_gnome("Teams/Circle"),
        ),
        (
            TeamId("Release Team".into()),
            Repo::for_gnome("GNOME/gnome-build-meta"),
        ),
    ]
}

pub async fn core_components() -> Vec<(ComponentId, Repo)> {
    let mut modules = BTreeSet::new();
    for subdir in CORE_FOLDERS.iter() {
        let dir = PathBuf::from_iter([GNOME_BUILD_META_DIR, subdir]);
        for entry in std::fs::read_dir(dir).unwrap() {
            let path = entry.unwrap().path();
            let file = File::open(&path).unwrap();
            let bst_element: BstElement = serde_yaml::from_reader(file).unwrap();
            if let Some(url) = bst_element
                .sources
                .and_then(|x| x.first().cloned())
                .and_then(|x| x.url)
            {
                let (location, element) = url.split_once(":").unwrap();
                if location == "gnome" {
                    let module = element.strip_suffix(".git").unwrap_or(location);
                    modules.insert(module.to_string());
                }
            }
        }
    }

    modules
        .into_iter()
        .map(|x| {
            (
                ComponentId(x.clone()),
                Repo::for_gnome(format!("GNOME/{x}")),
            )
        })
        .collect()
}

#[derive(Debug, Serialize, Deserialize)]
pub struct CircleComponent {
    repo: String,
}

pub async fn circle_components() -> Vec<(ComponentId, Repo)> {
    let circle_data: tokio_util::bytes::Bytes = reqwest::Client::new()
        .get(CIRCLE_URL)
        .send()
        .await
        .unwrap()
        .bytes()
        .await
        .unwrap();

    let list: BTreeMap<String, CircleComponent> = serde_yaml::from_slice(&circle_data)
        .with_context(|| format!("{circle_data:?}"))
        .unwrap();

    list.into_iter()
        .map(|(id, data)| {
            let repo = Repo::from_url(&data.repo).unwrap();

            (ComponentId(id), repo)
        })
        .collect()
}

pub async fn project_infos<T: IntoProjectId>(
    components: Vec<(T, Repo)>,
    args: Arc<Args>,
    gnome_category: GnomeCategory,
) -> Vec<(git_repos::ProjectInfo, T, Repo, GnomeCategory)> {
    let mut futures = JoinSet::new();
    for (project_id, repo) in components {
        let args = args.clone();
        let x = project_id.clone().into_project_id();
        futures.spawn(async move {
            (
                git_users::project_info(&x, &repo, args).await,
                project_id,
                repo,
            )
        });
    }

    let mut project_infos = Vec::new();
    while let Some(result) = futures.join_next().await {
        let (info, id, repo) = result.unwrap();
        project_infos.push((info.unwrap(), id, repo, gnome_category));
    }

    project_infos
}
