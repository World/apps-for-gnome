use std::sync::{Arc, RwLock};

use gitlab::api::{projects, AsyncQuery};
use libappstream::prelude::*;
use page_tools::Person;
use serde::Deserialize;

use super::doap;
use super::git_users::ProjectId;
use crate::{cli, log_warn, utils};

static APP_USER_AGENT: &str = concat!(env!("CARGO_PKG_NAME"), "/", env!("CARGO_PKG_VERSION"),);

#[derive(Debug, Clone)]
pub struct ProjectInfo {
    pub doap: doap::Document,
    pub repo_info: ProjectApi,
    pub maintainers: Vec<Arc<RwLock<Person>>>,
}

#[derive(Debug, Eq, PartialEq, PartialOrd, Ord, Clone)]
pub enum Repo {
    GitLab(String, String),
    Forgejo(String, String),
    GitHub(String),
}

impl Repo {
    pub fn for_gnome(repo: impl ToString) -> Self {
        Self::GitLab("gitlab.gnome.org".into(), repo.to_string())
    }
}

#[derive(Debug, Clone, Deserialize, Default)]
pub struct ProjectApi {
    pub path: Option<String>,
    pub default_branch: String,
    avatar_url: Option<String>,
    pub topics: Vec<String>,
    #[serde(default)]
    organization: Organization,
}

#[derive(Debug, Clone, Deserialize, Default)]
pub struct Organization {
    pub avatar_url: Option<String>,
}

impl ProjectApi {
    pub fn avatar_url(&self) -> Option<String> {
        self.avatar_url
            .to_owned()
            .or_else(|| self.organization.avatar_url.to_owned())
    }
}

impl Repo {
    pub async fn from_appstream(
        component: utils::UnsafeSend<libappstream::Component>,
    ) -> Option<Self> {
        let vcs_browser = component.url(libappstream::UrlKind::VcsBrowser);
        let bugtracker = component.url(libappstream::UrlKind::Bugtracker);
        let homepage = component.url(libappstream::UrlKind::Homepage);

        if vcs_browser.is_none() {
            log_warn!(
                component.id().unwrap(),
                "Missing `<url type=\"vcs-browser\">`",
            );
        }

        if let Some(url) = vcs_browser
            .as_ref()
            .or(bugtracker.as_ref())
            .or(homepage.as_ref())
        {
            let original_repo = Self::from_url(url);
            let redirect_repo = if let Some(original_repo) = &original_repo {
                Self::from_url(reqwest::get(original_repo.url()).await.ok()?.url().as_ref())
            } else {
                None
            };

            if original_repo != redirect_repo {
                log_warn!(
                    &component.id().unwrap(),
                    "Git repository has moved: from '{:?}' to '{:?}'",
                    url.to_string(),
                    redirect_repo
                );
            }

            if redirect_repo.is_some() {
                return redirect_repo;
            } else if original_repo.is_some() {
                return original_repo;
            }
        }

        log_warn!(
            component.id().unwrap(),
            "Not able to derive repository location from URLs. Vcs Browser: {vcs_browser:?}, Homepage: {homepage:?}, Bugtracker {bugtracker:?}",
        );
        None
    }

    pub fn from_url(url: &str) -> Option<Self> {
        if let Some((_, host, path, _)) =
            regex_captures!("^https://([^/]+)/(.+?)/(?:-/)?issues(/new)?", url)
        {
            if host == "github.com" {
                Some(Self::GitHub(path.to_string()))
            } else {
                Some(Self::GitLab(host.to_string(), path.to_string()))
            }
        } else if let Some((_, path)) = regex_captures!("^https://github.com/(.+?)/?$", url) {
            Some(Self::GitHub(path.to_string()))
        } else if let Some((_, path)) = regex_captures!("^https://codeberg.org/(.+?)/?$", url) {
            Some(Self::Forgejo("codeberg.org".to_string(), path.to_string()))
        } else if let Some((_, host, path)) = regex_captures!("^https://([^/]+)/(.+?)/?$", url) {
            Some(Self::GitLab(host.to_string(), path.to_string()))
        } else {
            None
        }
    }

    pub fn name(&self) -> String {
        match self {
            Self::GitLab(_, name) | Self::Forgejo(_, name) | Self::GitHub(name) => name,
        }
        .clone()
    }

    pub fn project_name(&self) -> String {
        self.name().split('/').last().unwrap().to_string()
    }

    pub fn url(&self) -> String {
        match self {
            Self::GitLab(url, name) | Self::Forgejo(url, name) => {
                format!("https://{}/{}", url, name)
            }
            Self::GitHub(name) => format!("https://github.com/{}", name),
        }
    }

    pub async fn fetch_doap_and_repo_info(
        &self,
        app_id: &ProjectId,
        args: Arc<cli::Args>,
    ) -> color_eyre::Result<(doap::Document, ProjectApi)> {
        debug!("Trying to get .doap file from {:?}", self);

        let doap_file;
        let project_api;

        match self {
            Repo::GitLab(host, path_with_namespace) => {
                let client = gitlab::GitlabBuilder::new_unauthenticated(host)
                    .build_async()
                    .await?;

                let ep = gitlab::api::projects::Project::builder()
                    .project(path_with_namespace.as_str())
                    .build()?;

                let project: ProjectApi = ep.query_async(&client).await?;

                let ep = projects::repository::files::FileRaw::builder()
                    .project(path_with_namespace.as_str())
                    .file_path(format!("{}.doap", project.path.as_ref().unwrap()))
                    .ref_(&project.default_branch)
                    .build()?;

                doap_file =
                    String::from_utf8_lossy(&gitlab::api::raw(ep).query_async(&client).await?)
                        .to_string();
                project_api = project;
            }
            repo @ Repo::Forgejo(_, _) => {
                let url = format!(
                    "{}/raw/branch/HEAD/{}.doap",
                    repo.url(),
                    self.project_name()
                );

                let client = reqwest::Client::builder()
                    .user_agent(APP_USER_AGENT)
                    .build()?
                    .get(url);

                doap_file = client.send().await?.text().await?;
                project_api = ProjectApi::default();
            }
            Repo::GitHub(path_with_namespace) => {
                let url = format!(
                    "https://api.github.com/repos/{}/contents/{}.doap",
                    path_with_namespace,
                    self.project_name()
                );

                let mut client = reqwest::Client::builder()
                    .user_agent(APP_USER_AGENT)
                    .build()?
                    .get(url)
                    .header(
                        reqwest::header::ACCEPT,
                        "application/vnd.github.VERSION.raw",
                    );

                if let Some(token) = &args.github_token {
                    client =
                        client.header(reqwest::header::AUTHORIZATION, format!("token {token}"));
                }

                doap_file = client.send().await?.text().await?;

                let url = format!("https://api.github.com/repos/{path_with_namespace}");

                let mut client = reqwest::Client::builder()
                    .user_agent(APP_USER_AGENT)
                    .build()?
                    .get(url);

                if let Some(token) = &args.github_token {
                    client =
                        client.header(reqwest::header::AUTHORIZATION, format!("token {token}"));
                }

                project_api = match client.send().await?.json().await {
                    Ok(project_api) => project_api,
                    Err(err) => {
                        log_warn!(
                            app_id,
                            "Could not retrieve GitHub data for component {self:?}: {err}"
                        );
                        ProjectApi::default()
                    }
                };
            }
        };

        let doap_document = match serde_xml_rs::from_str(&doap_file) {
            Ok(doap_document) => doap_document,
            Err(err) => {
                log_warn!(app_id, "Could not retrieve doap for component {self:?}: {err}\nContent was: {doap_file}");
                doap::Document::default()
            }
        };

        Ok((doap_document, project_api))
    }
}
