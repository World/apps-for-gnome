use serde::Deserialize;

use super::git_users::GitAccountInternal;
use crate::lists;

#[derive(Debug, Deserialize, Clone, Default)]
pub struct Document {
    pub name: Option<String>,
    pub shortdesc: Option<String>,
    pub description: Option<String>,
    pub homepage: Option<RdfResource>,
    #[serde(default)]
    pub repository: Repositories,
    #[serde(rename = "maintainer", default)]
    pub maintainers: Vec<Maintainer>,
    #[serde(rename = "programming-language", default)]
    pub programming_languages: Vec<String>,
    #[serde(rename = "platform", default)]
    pub platforms: Vec<String>,
}

#[derive(Debug, Deserialize, Clone)]
pub struct RdfResource {
    pub resource: String,
}

#[derive(Debug, Deserialize, Clone, Default)]
pub struct Repositories {
    #[serde(rename = "$value")]
    pub repositories: Vec<GitRepository>,
}

#[derive(Debug, Deserialize, Clone)]
pub struct GitRepository {
    pub browse: Option<RdfResource>,
}

#[derive(Debug, Deserialize, Clone)]
pub struct Maintainer {
    #[serde(rename = "Person")]
    pub person: Person,
}

impl Maintainer {
    pub async fn git_accounts(&self) -> Vec<GitAccountInternal> {
        let mut accounts = Vec::new();

        let ldap_to_gitlab = lists::ldap_to_gitlab().await;
        if let Some(gitlab_user) = self
            .person
            .userid
            .as_ref()
            .and_then(|u| ldap_to_gitlab.get(u))
        {
            accounts.push(GitAccountInternal::new(
                String::from("gitlab.gnome.org"),
                gitlab_user.to_string(),
            ));
        }

        let mut explicit_accounts = self
            .person
            .account
            .iter()
            .map(|account| {
                GitAccountInternal::new(
                    url::Url::parse(&account.online_account.account_service_homepage.resource)
                        .unwrap()
                        .host_str()
                        .unwrap()
                        .to_string(),
                    account.online_account.account_name.to_string(),
                )
            })
            .collect();

        accounts.append(&mut explicit_accounts);

        accounts
    }
}

#[derive(Debug, Deserialize, Clone)]
pub struct Person {
    pub name: Option<String>,
    pub userid: Option<String>,
    #[serde(default)]
    pub account: Vec<Account>,
}

#[derive(Debug, Deserialize, Clone)]
pub struct Account {
    #[serde(rename = "OnlineAccount")]
    pub online_account: OnlineAccount,
}

#[derive(Debug, Deserialize, Clone)]
#[serde(rename_all = "camelCase")]
pub struct OnlineAccount {
    pub account_service_homepage: AccountServiceHomepage,
    pub account_name: String,
}

#[derive(Debug, Deserialize, Clone)]
pub struct AccountServiceHomepage {
    pub resource: String,
}
