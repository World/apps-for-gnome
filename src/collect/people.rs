use std::collections::BTreeMap;
use std::sync::{Arc, RwLock};

use once_cell::sync::Lazy;
use page_tools::{People, Person};

use super::git_users::GitAccountInternal;

pub static USERS: Lazy<RwLock<BTreeMap<GitAccountInternal, Arc<RwLock<Person>>>>> =
    Lazy::new(Default::default);

pub fn people() -> People {
    let list: Vec<_> = USERS
        .read()
        .unwrap()
        .values()
        .map(|x| x.read().unwrap().clone())
        .collect();

    People::from(list)
}
