use std::fmt::Display;
use std::sync::{Arc, RwLock};

use color_eyre::eyre::bail;
use gitlab::api::AsyncQuery;
use page_tools::{AppId, ComponentId, GitAccount, Person, SocialAccount, TeamId};
use serde::Deserialize;

use super::git_repos::{ProjectInfo, Repo};
use super::{doap, people};
use crate::{cli, log_warn};

static APP_USER_AGENT: &str = concat!(env!("CARGO_PKG_NAME"), "/", env!("CARGO_PKG_VERSION"),);

#[derive(Debug, Clone, PartialEq, PartialOrd, Ord, Eq)]
pub struct GitAccountInternal {
    host: String,
    username: String,
}

impl GitAccountInternal {
    pub fn new(host: String, username: String) -> Self {
        GitAccountInternal { host, username }
    }

    pub fn is_github(&self) -> bool {
        self.host == "github.com"
    }
}

#[derive(Debug)]
pub enum ProjectId {
    App(AppId),
    Component(ComponentId),
    Team(TeamId),
}

pub trait IntoProjectId: Send + Clone + 'static {
    fn into_project_id(self) -> ProjectId;
}

impl IntoProjectId for ComponentId {
    fn into_project_id(self) -> ProjectId {
        ProjectId::Component(self)
    }
}

impl IntoProjectId for TeamId {
    fn into_project_id(self) -> ProjectId {
        ProjectId::Team(self)
    }
}

impl Display for ProjectId {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::App(id) => {
                f.write_str("app:")?;
                f.write_str(&id.to_string())
            }
            Self::Component(component) => {
                f.write_str("component:")?;
                f.write_str(&component.to_string())
            }
            Self::Team(team) => {
                f.write_str("team:")?;
                f.write_str(&team.to_string())
            }
        }
    }
}

#[derive(Debug, Deserialize)]
struct PersonApi {
    id: u64,
    #[serde(alias = "login")]
    username: String,
    name: Option<String>,
    pronouns: Option<String>,
    location: Option<String>,
    /// Account URL
    #[serde(alias = "html_url")]
    web_url: String,
    avatar_url: String,
    /// GitLab
    #[serde(alias = "blog")]
    website_url: Option<String>,
    #[serde(default)]
    social_accounts: Vec<SocialAccountApi>,
}

impl PersonApi {
    fn normalize(&mut self) {
        if self.website_url.as_ref().map_or(false, |x| x.is_empty()) {
            self.website_url = None;
        } else if let Some(website_url) = &mut self.website_url {
            if !website_url.contains("://") {
                *website_url = format!("https://{website_url}");
            }
        }

        if let Ok(mut url) = url::Url::parse(&self.avatar_url) {
            if url.domain().map_or(false, |x| x.contains("gravatar.com")) {
                // Fix gravatar usually delivering a pretty small image
                url.query_pairs_mut().append_pair("s", "220");

                self.avatar_url = url.to_string();
            }
        }
    }
}

#[derive(Debug, Deserialize, Clone)]
pub struct SocialAccountApi {
    provider: String,
    url: url::Url,
}

impl SocialAccountApi {
    pub fn into_social_account(self) -> Option<SocialAccount> {
        let display = match self.provider.as_str() {
            "mastodon" => {
                if let (Some(host), Some((_, username))) =
                    (self.url.host_str(), self.url.path().split_once('@'))
                {
                    format!("@{username}@{host}")
                } else {
                    self.url.to_string()
                }
            }
            _ => return None,
        };

        Some(SocialAccount {
            provider: self.provider,
            url: self.url.to_string(),
            display,
        })
    }
}

async fn get_user_github(git_user: &str, opt: &cli::Args) -> color_eyre::Result<PersonApi> {
    let url = format!("https://api.github.com/users/{git_user}");

    let mut client = reqwest::Client::builder()
        .user_agent(APP_USER_AGENT)
        .build()?
        .get(url);

    if let Some(token) = &opt.github_token {
        client = client.header(reqwest::header::AUTHORIZATION, format!("token {}", token));
    }

    let mut person: PersonApi = client.send().await?.json().await?;

    let url = format!("https://api.github.com/users/{git_user}/social_accounts");

    let mut client = reqwest::Client::builder()
        .user_agent(APP_USER_AGENT)
        .build()?
        .get(url);

    if let Some(token) = &opt.github_token {
        client = client.header(reqwest::header::AUTHORIZATION, format!("token {}", token));
    }

    let social_accounts: Vec<SocialAccountApi> = client.send().await?.json().await?;

    person.social_accounts = social_accounts;

    Ok(person)
}

async fn get_user_gitlab(
    git_account: &GitAccountInternal,
    opt: &cli::Args,
) -> color_eyre::Result<PersonApi> {
    let client = gitlab_client(&git_account.host, opt).await?;

    let git_user = &git_account.username;

    let ep = gitlab::api::users::Users::builder()
        .username(git_user)
        .build()?;

    let user: Option<PersonApi> = gitlab::api::paged(ep, gitlab::api::Pagination::Limit(1))
        .query_async(&client)
        .await?
        .pop();

    if let Some(user) = user {
        let ep = gitlab::api::users::User::builder().user(user.id).build()?;

        Ok(ep.query_async(&client).await.ok().unwrap_or(user))
    } else {
        bail!("Could not retrieve user {git_user} from GitLab.");
    }
}

async fn gitlab_client(git_host: &str, opt: &cli::Args) -> color_eyre::Result<gitlab::AsyncGitlab> {
    if let Some(token) = opt
        .gitlab_token
        .iter()
        .flatten()
        .find(|token| token.host == git_host)
    {
        gitlab::GitlabBuilder::new(git_host, &token.token)
    } else {
        gitlab::GitlabBuilder::new_unauthenticated(git_host)
    }
    .build_async()
    .await
    .map_err(Into::into)
}

fn merge_person(p1: &mut Person, mut p2: PersonApi) {
    if p2.username == "marge-bot" {
        return;
    }

    p2.normalize();

    if p1.name.is_none() {
        p1.name = p2.name;
    }

    if p1.pronouns.is_none() {
        p1.pronouns = p2.pronouns;
    }

    if p1.avatar_url.is_none() {
        p1.avatar_url = Some(p2.avatar_url);
    }

    if p1.location.is_none() {
        p1.location = p2.location;
    }

    if let Some(website) = p2.website_url {
        p1.websites.insert(website);
    }

    p1.social_accounts.append(
        &mut p2
            .social_accounts
            .into_iter()
            .filter_map(|x| x.into_social_account())
            .collect(),
    );

    p1.git_accounts.insert(GitAccount {
        url: p2.web_url,
        username: p2.username,
    });
}

pub async fn project_info(
    project: &ProjectId,
    repo: &Repo,
    opt: Arc<cli::Args>,
) -> color_eyre::Result<ProjectInfo> {
    let (doap, repo_info) = repo.fetch_doap_and_repo_info(project, opt.clone()).await?;

    let maintainers = maintainers(&doap, project, &opt).await;

    Ok(ProjectInfo {
        doap,
        repo_info,
        maintainers,
    })
}

pub async fn maintainers(
    doap: &doap::Document,
    project: &ProjectId,
    opt: &cli::Args,
) -> Vec<Arc<RwLock<Person>>> {
    let mut maintainers = Vec::new();

    for maintainer in &doap.maintainers {
        let git_accounts = maintainer.git_accounts().await;

        let person = git_accounts
            .iter()
            .find_map(|x| people::USERS.read().unwrap().get(x).cloned())
            .unwrap_or_default();

        if let Some(user_id) = &maintainer.person.userid {
            person.write().unwrap().gnome_account = Some(user_id.clone());
        };

        for git_account in git_accounts {
            {
                let mut people = people::USERS.write().unwrap();
                if people.contains_key(&git_account) {
                    continue;
                } else {
                    people.insert(git_account.clone(), person.clone());
                }
            }

            let git_user = &git_account.username;
            let git_url = &git_account.host;

            let new_person = if git_account.is_github() {
                get_user_github(git_user, opt).await
            } else {
                get_user_gitlab(&git_account, opt).await
            };

            match new_person {
                Ok(p2) => {
                    let mut mut_person = person.write().unwrap();
                    merge_person(&mut mut_person, p2);
                }
                Err(err) => log_warn!(
                    project,
                    "Failed to retrieve maintainer info '{git_user}' at '{git_url}': {err}"
                ),
            }
        }

        match project {
            ProjectId::App(app_id) => {
                person.write().unwrap().apps.insert(app_id.clone());
            }
            ProjectId::Component(component) => {
                person.write().unwrap().components.insert(component.clone());
            }
            ProjectId::Team(team) => {
                person.write().unwrap().teams.insert(team.clone());
            }
        }

        maintainers.push(person);
    }

    maintainers
}
