use std::collections::{BTreeMap, HashMap};
use std::fs::File;
use std::path::PathBuf;
use std::sync::Arc;

use chrono::prelude::*;
use color_eyre::eyre::WrapErr;
use page_tools::AppId;
use serde::{Deserialize, Serialize};
use tokio::sync::Semaphore;

#[derive(Debug, Default, Deserialize, Serialize, Clone)]
pub struct Stats {
    pub start_date: chrono::NaiveDate,
    pub entries: BTreeMap<chrono::NaiveDate, StatsEntry>,
}

#[derive(Debug, Default, Clone)]
pub struct AppStats {
    start_date: chrono::NaiveDate,
    installs: u64,
    updates: Option<u64>,
}

impl AppStats {
    pub fn installs_per_day(&self) -> f64 {
        let stats_range = chrono::Local::now().date_naive() - self.start_date;
        self.installs as f64 / stats_range.num_days() as f64
    }

    pub fn updates_since_last_release_total(&self) -> Option<u64> {
        self.updates
    }
}

impl Stats {
    pub async fn download(start_date: chrono::NaiveDate) -> Self {
        let mut entries: BTreeMap<chrono::NaiveDate, StatsEntry> = Default::default();
        let semaphore = Arc::new(Semaphore::new(16));

        let mut tasks = tokio::task::JoinSet::new();

        let last_day = chrono::Local::now().date_naive() - chrono::Duration::days(2);
        for day in start_date.iter_days() {
            if day > last_day {
                break;
            }
            tasks.spawn(StatsEntry::download(day, semaphore.clone()));
        }

        while let Some(res) = tasks.join_next().await {
            let (date, entry) = res.unwrap();
            entries.insert(date, entry);
        }

        Self {
            start_date,
            entries,
        }
    }

    pub fn app_stats(&self, app_id: &AppId, last_release: &chrono::NaiveDate) -> AppStats {
        let installs = self.installs_since(app_id, &self.start_date);
        let updates = self.updates_since(app_id, last_release);

        AppStats {
            start_date: self.start_date,
            installs,
            updates,
        }
    }

    fn updates_since(&self, app_id: &AppId, since: &chrono::NaiveDate) -> Option<u64> {
        if *since < self.start_date {
            return None;
        }

        let mut total = 0;
        for (_, entry) in self.entries.iter().filter(|x| x.0 >= since) {
            total += entry.refs.get(app_id).map_or(0, |x| x.updates());
        }

        Some(total)
    }

    fn installs_since(&self, app_id: &AppId, since: &chrono::NaiveDate) -> u64 {
        let mut total = 0;
        for (_, entry) in self.entries.iter().filter(|x| x.0 >= since) {
            total += entry.refs.get(app_id).map_or(0, |x| x.installs());
        }

        total
    }
}

#[derive(Debug, Default, Deserialize, Serialize, Clone)]
pub struct StatsEntry {
    pub refs: BTreeMap<AppId, Arches>,
}

impl StatsEntry {
    pub async fn download(
        date: chrono::NaiveDate,
        semaphore: Arc<Semaphore>,
    ) -> (chrono::NaiveDate, Self) {
        let permit = semaphore.acquire().await.unwrap();

        let filename = format!("{}/{:02}/{:02}.json", date.year(), date.month(), date.day());
        let cache_name = format!(
            "stats-{}-{:02}-{:02}.mpk",
            date.year(),
            date.month(),
            date.day()
        );
        let path = PathBuf::from_iter(["cache", cache_name.as_str()]);
        let path_ = path.clone();

        let entry = if let Some(stats) = tokio::task::spawn_blocking(move || {
            File::open(&path_)
                .ok()
                .and_then(|x| rmp_serde::from_read(x).ok())
        })
        .await
        .unwrap()
        {
            debug!("Using cached {path:?}");
            stats
        } else {
            let url = format!("https://flathub.org/stats/{filename}");

            debug!("Downloading {url}");
            let stats = reqwest::get(&url)
                .await
                .unwrap()
                .json()
                .await
                .context(url.clone())
                .unwrap();
            drop(permit);

            debug!("Completed {url}, writing cache to {path:?}");
            std::fs::write(&path, rmp_serde::to_vec(&stats).unwrap()).unwrap();
            stats
        };

        (date, entry)
    }
}

impl std::ops::Add for StatsEntry {
    type Output = Self;

    fn add(mut self, other: Self) -> Self {
        for (key, value) in other.refs.into_iter() {
            let entry = self.refs.entry(AppId::new(key)).or_default();
            *entry = entry.clone() + value;
        }

        self
    }
}

#[derive(Debug, Deserialize, Serialize, Default, Clone)]
pub struct Arches(HashMap<String, Point>);

impl Arches {
    pub fn total(&self) -> Point {
        let mut total = Point::default();
        for value in self.0.values() {
            total = total.clone() + value.clone();
        }

        total
    }

    #[allow(dead_code)]
    pub fn updates(&self) -> u64 {
        self.total().updates()
    }

    pub fn installs(&self) -> u64 {
        self.total().installs()
    }
}

impl std::ops::Add for Arches {
    type Output = Self;

    fn add(mut self, other: Self) -> Self {
        for (key, value) in other.0.into_iter() {
            let entry = self.0.entry(key).or_default();
            *entry = entry.clone() + value;
        }

        self
    }
}

#[derive(Debug, Deserialize, Serialize, Default, Clone)]
pub struct Point(u64, u64);

impl Point {
    #[allow(dead_code)]
    pub fn updates(&self) -> u64 {
        self.1
    }

    pub fn installs(&self) -> u64 {
        self.0 - self.1
    }
}

impl std::ops::Add for Point {
    type Output = Self;

    fn add(self, other: Self) -> Self {
        Self(self.0 + other.0, self.1 + other.1)
    }
}
