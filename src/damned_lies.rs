//! https://l10n.gnome.org/api/v1/
//! https://wiki.gnome.org/DamnedLies
use libappstream::prelude::*;
use page_tools::TranslationStats;
use serde::Deserialize;

use super::log_warn;
use crate::collect::git_repos;
use crate::error::*;
use crate::utils::UnsafeSend;

#[derive(Deserialize, Debug)]
pub struct Module {
    name: String,
    branches: Vec<Branch>,
}

#[derive(Deserialize, Debug)]
pub struct Branch {
    name: String,
}
#[derive(Deserialize, Debug)]
pub struct BranchStats {
    domains: Vec<Domain>,
}
#[derive(Deserialize, Debug)]
pub struct Domain {
    name: String,
    statistics: std::collections::HashMap<String, Strings>,
}
#[derive(Deserialize, Debug)]
pub struct Strings {
    trans: u64,
    fuzzy: u64,
    untrans: u64,
}

impl Strings {
    pub fn percentage(&self) -> Result<u32, &str> {
        if self.total() == 0 {
            Err("No translation strings found in d-l")
        } else {
            Ok((self.trans * 100 / self.total()) as u32)
        }
    }

    pub fn total(&self) -> u64 {
        self.trans + self.fuzzy + self.untrans
    }

    pub fn as_appstream_language(&self, lang: &str) -> Result<TranslationStats, &str> {
        Ok(TranslationStats {
            locale: lang.to_string(),
            percentage: self.percentage().ok(),
        })
    }
}

#[derive(Debug, Clone)]
pub struct DamnedLiesModule {
    pub name: String,
    pub stats: Vec<TranslationStats>,
    pub strings: Option<u64>,
}

pub async fn stats(
    app: &UnsafeSend<libappstream::Component>,
    repo: &Option<git_repos::Repo>,
) -> color_eyre::Result<DamnedLiesModule> {
    let mut potential_module_names = Vec::new();

    if let Some(module_name) = app.translations().first().and_then(|x| x.id()) {
        potential_module_names.push(module_name.to_string())
    }

    if let Some(repo) = repo {
        potential_module_names.push(repo.name().split('/').last().unwrap().to_string())
    }

    let mut module_iter = potential_module_names.iter();
    let module: Module = loop {
        if let Some(module_name) = module_iter.next() {
            let module_url = format!("https://l10n.gnome.org/api/v1/modules/{}", module_name);
            match reqwest::get(module_url).await?.json().await {
                Ok(module) => {
                    break module;
                }
                Err(_) => {
                    continue;
                }
            }
        } else {
            return Err(AppError::from_app(
                app,
                &format!(
                    "d-l: Module not found: {:?} - {:?}",
                    potential_module_names, repo
                ),
            )
            .into());
        }
    };

    let branch = if let Some(branch) = module.branches.get(1) {
        &branch.name
    } else if let Some(branch) = module.branches.first() {
        &branch.name
    } else {
        return Err(AppError::from_app(app, "d-l: No branch available.").into());
    };

    let stats_url = format!(
        "https://l10n.gnome.org/api/v1/modules/{}/branch/{}",
        module.name, branch
    );

    let stats: BranchStats = reqwest::get(stats_url).await?.json().await?;

    if let Some(domain) = stats.domains.first() {
        if &domain.name != "po" {
            log_warn!(app.id().unwrap(), "Domain name is not 'po'.");
        }

        let strings = domain.statistics.values().next().map(|x| x.total());

        let stats = domain
            .statistics
            .iter()
            .filter_map(|(lang, strings)| {
                strings
                    .as_appstream_language(lang)
                    .warn(app.id().unwrap().to_string())
                    .ok()
            })
            .collect();

        Ok(DamnedLiesModule {
            name: module.name,
            stats,
            strings,
        })
    } else {
        Err(AppError::from_app(app, "d-l: No translation domain available.").into())
    }
}
