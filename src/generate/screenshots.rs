use futures::prelude::*;
use libappstream::prelude::*;
use tokio_util::compat::FuturesAsyncReadCompatExt;

use crate::collect::AppRaw;
use crate::log_warn;
use crate::utils::new_file_tokio;

pub async fn generate(apps: &[AppRaw]) {
    let mut tasks = tokio::task::JoinSet::new();
    for app in apps.iter() {
        for screenshot in &app.appstream.screenshots_all() {
            let image = screenshot
                .images_all()
                .into_iter()
                .find(|x| x.kind() == libappstream::ImageKind::Source);

            if let Some(img_url) = image.and_then(|x| x.url()) {
                let app_id = app.id.to_string();
                tasks.spawn(async move {
                    debug!("Getting screenshot: {}", img_url.as_str());
                    match reqwest::get(img_url.to_string()).await {
                        Ok(img_data) => {
                            debug!("Got screenshot reply: {}", img_url.as_str());

                            let content_type = img_data.headers().get("Content-Type");
                            if content_type.map(|x| x.as_bytes()) != Some(b"image/png".as_ref()) {
                                log_warn!(
                                    app_id,
                                    "Wrong content type `{:?}` for screenshot: {}",
                                    content_type,
                                    img_url
                                );
                            } else {
                                let url = url::Url::parse(&img_url).unwrap();
                                let filename = url.path_segments().and_then(|x| x.last()).unwrap();
                                let mut file = new_file_tokio(format!(
                                    "assets/screenshots/{}/{}",
                                    app_id, filename
                                ))
                                .await
                                .unwrap();
                                let mut data = img_data
                                    .bytes_stream()
                                    .map_err(|e| {
                                        futures::io::Error::new(futures::io::ErrorKind::Other, e)
                                    })
                                    .into_async_read()
                                    .compat();

                                //copy_to
                                tokio::io::copy(&mut data, &mut file).await.unwrap();
                            }
                        }
                        Err(_) => {
                            log_warn!(app_id, "Screenshot not available: {}", img_url)
                        }
                    }
                });
            }
        }
    }

    while let Some(task) = tasks.join_next().await {
        task.unwrap();
    }
}
