use std::collections::HashMap;
use std::fmt::Display;

use libappstream::prelude::*;
use once_cell::sync::Lazy;
use page_tools::{i18n, AppId};
use serde::{Deserialize, Serialize};

use super::collect::*;
use crate::utils::{escape_markup, UnsafeSend};
use crate::{log_warn, utils};

pub type AppOut = page_tools::App;
pub type ReleaseOut = page_tools::Release;
pub type ScreenshotOut = page_tools::Screenshot;

pub static APP_ID_EXCEPTIONS: Lazy<HashMap<String, String>> = Lazy::new(|| {
    [
        ("org.gnome.Baobab", "org.gnome.baobab"),
        ("org.gnome.Clocks", "org.gnome.clocks"),
        ("org.gnome.DconfEditor", "ca.desrt.dconf-editor"),
        ("org.gnome.DSpy", "org.gnome.dspy"),
        ("org.gnome.SysprofApp", "org.gnome.Sysprof"),
        ("org.gnome.FontViewer", "org.gnome.font-viewer"),
        ("org.gnome.Yelp", "yelp"),
    ]
    .iter()
    .map(|(x, y)| (x.to_string(), y.to_string()))
    .collect()
});

pub trait Out<T> {
    fn out(&self, lang: &str) -> T;
}

#[derive(Debug, Serialize, Deserialize)]
pub struct MyContext {
    pub apps: Vec<AppOut>,
}

impl Out<Vec<AppOut>> for Vec<AppRaw> {
    fn out(&self, lang: &str) -> Vec<AppOut> {
        let mut apps: Vec<AppOut> = self.iter().map(|x| x.out(lang)).collect();
        let collator = page_tools::i18n::collator(lang);
        apps.sort_by(|a, b| collator.compare(&a.name, &b.name));
        apps
    }
}

pub fn real_id(app_id: impl Display) -> AppId {
    let mut id = app_id.to_string();

    if let Some(real_id) = id.strip_suffix(".desktop") {
        id = real_id.into();
        log_warn!(id, "Deprecated '.desktop' suffix in app id '{app_id}'");
    }

    if let Some(new_id) = APP_ID_EXCEPTIONS.get(&id) {
        id = new_id.clone();
    }

    AppId::new(id)
}

fn keyword_collection(
    app: &AppRaw,
    appstream: &UnsafeSend<libappstream::Component>,
    lang: &str,
    app_id: &AppId,
) -> Vec<String> {
    let mut collection: Vec<String> = Vec::new();

    // Keywords
    // TODO: get only translated and en
    //collection.extend(dbg!(appstream.keywords().iter().map(|x| x.to_string())));

    // Categories
    collection.extend(appstream.categories().iter().map(|x| x.to_string()));

    // App name
    collection.extend(
        [
            &app.appstream.name().map(|x| x.to_string()),
            &appstream.name().map(|x| x.to_string()),
        ]
        .into_iter()
        .flatten()
        .cloned(),
    );

    // Linux
    // TODO: Translating "Linux" here does not work
    collection.extend(["Linux".into(), i18n::translate("Linux")]);

    collection
        .iter_mut()
        .for_each(|x| *x = crate::utils::uppercase_first_letter(x));

    collection.sort();
    collection.dedup();

    collection
}

impl Out<AppOut> for AppRaw {
    fn out(&self, lang: &str) -> AppOut {
        let app = self;
        let app_id = real_id(app.appstream.id().unwrap().to_string());
        let page_id = crate::utils::page_id(&app_id);

        let appstream = utils::libappstream_component(&self.pool, &app_id).unwrap();

        let mut members = Vec::new();

        if let Some(project_info) = &app.git_project {
            for maintainer in &project_info.maintainers {
                let read_member = maintainer.read().unwrap();
                members.push(read_member.clone());
            }
        }

        let (screenshots_light, screenshots_dark) = self.screenshots();

        let display_size = appstream
            .requires()
            .into_iter()
            .find(|x| x.item_kind() == libappstream::RelationItemKind::DisplayLength)
            .map(|x| x.value_px());
        let mobile_support = if let Some(display_size) = display_size {
            display_size < 361
        } else {
            log_warn!(app_id, "No `<requires><display_length ...` specified");
            false
        };

        let keyword_collection = keyword_collection(app, &appstream, lang, &app_id);

        AppOut {
            gnome: app.project.to_string(),
            id: app_id.clone(),
            page_id,
            nightly_id: app.nightly_id.clone(),
            name: appstream.name().map(|x| x.to_string()).unwrap_or_default(),
            name_ord: 0,
            summary: appstream
                .summary()
                .map(|x| x.to_string())
                .unwrap_or_default(),
            description: escape_markup(
                &appstream
                    .description()
                    .map(|x| x.to_string())
                    .unwrap_or_default(),
            ),
            project_license: appstream
                .project_license()
                .map(|x| x.to_string())
                .unwrap_or_default(),
            metadata_license: appstream
                .metadata_license()
                .map(|x| x.to_string())
                .unwrap_or_default(),
            members,

            releases: appstream
                .load_releases(true)
                .ok()
                .flatten()
                .map(|x| x.entries())
                .unwrap_or_default()
                .iter()
                .map(|x| x.out(lang))
                .collect(),
            languages: app
                .damned_lies
                .as_ref()
                .map(|x| x.stats.to_vec())
                .unwrap_or_else(|| {
                    appstream
                        .languages()
                        .iter()
                        .map(|x| page_tools::TranslationStats {
                            percentage: Some(appstream.language(Some(x)) as u32),
                            locale: x.to_string(),
                        })
                        .collect()
                }),
            dl_module: app.damned_lies.as_ref().map(|x| x.name.to_string()),
            dl_strings: app.damned_lies.as_ref().and_then(|x| x.strings),
            url_bugtracker: appstream
                .url(libappstream::UrlKind::Bugtracker)
                .map(|x| x.to_string()),
            url_contact: appstream
                .url(libappstream::UrlKind::Contact)
                .map(|x| x.to_string()),
            url_contribute: appstream
                .url(libappstream::UrlKind::Contribute)
                .map(|x| x.to_string()),
            url_donation: appstream
                .url(libappstream::UrlKind::Donation)
                .map(|x| x.to_string()),
            url_help: appstream
                .url(libappstream::UrlKind::Help)
                .map(|x| x.to_string()),
            url_homepage: appstream
                .url(libappstream::UrlKind::Homepage)
                .map(|x| x.to_string()),
            url_translate: appstream
                .url(libappstream::UrlKind::Translate)
                .map(|x| x.to_string()),
            url_vcs_browser: appstream
                .url(libappstream::UrlKind::VcsBrowser)
                .map(|x| x.to_string()),
            repo_url: app.repo.as_ref().map(|x| x.url()).unwrap_or_default(),
            repo_name: app.repo.as_ref().map(|x| x.name()).unwrap_or_default(),
            screenshots_light: screenshots_light.iter().map(|x| x.out(lang)).collect(),
            screenshots_dark: screenshots_dark.map(|x| x.iter().map(|x| x.out(lang)).collect()),
            categories: appstream
                .categories()
                .iter()
                .map(|x| x.to_string())
                .collect(),
            key_colors: appstream
                .custom_value("GnomeSoftware::key-colors")
                .map(|value| value.to_string()),
            brand_color_light: crate::utils::to_css_color(&app.brand_color_light),
            brand_color_dark: crate::utils::to_css_color(&app.brand_color_dark),
            text_color_light: crate::utils::to_css_color(&app.text_color_light),
            text_color_dark: crate::utils::to_css_color(&app.text_color_dark),
            on_flathub: app.on_flathub(),
            flathub_verified: app.flathub_verified(),
            flathub_last_build: app.flathub_summary.as_ref().map(|x| x.build_datetime()),
            flathub_stats_installs: app.flathub_stats.as_ref().map(|x| x.installs_per_day()),
            flathub_stats_updates: app
                .flathub_stats
                .as_ref()
                .and_then(|x| x.updates_since_last_release_total()),
            flathub_runtime: app.flathub_summary.as_ref().map(|x| x.runtime()),
            programming_languages: app
                .git_project
                .clone()
                .map(|x| x.doap.programming_languages)
                .unwrap_or_default(),
            gtk_version: app
                .git_project
                .as_ref()
                .map(|x| x.doap.platforms.clone())
                .iter()
                .flatten()
                .map(|x| x.to_lowercase().replace(' ', ""))
                .find(|x| x.starts_with("gtk"))
                .and_then(|x| x.chars().last())
                .and_then(|x| x.to_digit(10)),
            annotations: Vec::from_iter(
                crate::error::LOG
                    .load()
                    .get(&app_id.to_string())
                    .cloned()
                    .unwrap_or_default()
                    .clone(),
            ),
            mobile_support,
            keyword_collection,
        }
    }
}

impl Out<ReleaseOut> for libappstream::Release {
    fn out(&self, lang: &str) -> ReleaseOut {
        let lang = if lang.is_empty() { "en" } else { lang };

        let date = utils::libappstream_datetime(self.timestamp());

        ReleaseOut {
            date,
            locale_date: date.map(|date| page_tools::i18n::date(&date, lang)),
            version: self.version().map(|x| x.to_string()).unwrap_or_default(),
            description: self
                .description()
                .map(|x| x.to_string())
                .unwrap_or_default(),
            kind: self.kind().to_str().unwrap().to_lowercase(),
        }
    }
}

impl Out<ScreenshotOut> for libappstream::Screenshot {
    fn out(&self, _lang: &str) -> ScreenshotOut {
        let image = self
            .images_all()
            .into_iter()
            .find(|x| x.kind() == libappstream::ImageKind::Source)
            .unwrap();

        let file = url::Url::parse(&image.url().unwrap())
            .unwrap()
            .path_segments()
            .and_then(|x| x.last())
            .unwrap_or_default()
            .to_string();

        ScreenshotOut {
            default: self.kind() == libappstream::ScreenshotKind::Default,
            file,
            caption: self.caption().map(|x| x.to_string()).unwrap_or_default(),
            medium: String::from("image"),
        }
    }
}

trait Translated {
    fn translated(&self, lang: &str) -> String;
}

impl<T: Translated> Translated for Option<T> {
    fn translated(&self, lang: &str) -> String {
        if let Some(x) = self {
            x.translated(lang)
        } else {
            String::new()
        }
    }
}
