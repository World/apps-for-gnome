use std::io::Read;
use std::path::Path;

use color_eyre::eyre::WrapErr;
use itertools::Itertools;
use page_tools::AppId;
use palette::color_difference::Wcag21RelativeContrast;
use palette::{Darken, Lighten};
use rsvg::prelude::*;

fn light_default() -> palette::Srgb {
    palette::Srgb::from(0xffffff).into_format()
}

fn dark_default() -> palette::Srgb {
    palette::Srgb::from(0x241f31).into_format()
}

pub fn text_color_black(background_color: &palette::Srgb) -> bool {
    background_color.relative_contrast(dark_default())
        > background_color.relative_contrast(light_default())
}

pub fn text_color(background_color: &palette::Srgb) -> palette::Srgb {
    if background_color.relative_contrast(dark_default())
        > background_color.relative_contrast(light_default())
    {
        dark_default()
    } else {
        light_default()
    }
}

pub fn twitter(appid: &str, color: &palette::Srgb, section: &crate::collect::GnomeProject) {
    // 564 × 295 px
    let mut extra_code = format!(
        "<g transform='matrix(1, 0, 0, 1, -80, -280)'>{svg}</g>",
        svg = load_sub_svg("data/svg/background-rasterized.svg", 660 * 2, 660 * 2)
    );

    let style = if text_color_black(color) {
        "style='filter: invert(90%);'"
    } else {
        ""
    };

    extra_code += &format!(
        "<g {style} transform='matrix(1, 0, 0, 1, 866, 462)'>{svg}</g>",
        svg = load_sub_svg(
            &format!("data/svg/{}.svg", section.to_string()),
            115 * 2,
            48 * 2
        ),
        style = style,
    );

    generate_card(
        564 * 2,
        295 * 2,
        &format!("public.new/icons/scalable/{}.svg", appid),
        205 * 2,
        color,
        &format!("public.new/icons/twitter/{}.png", appid),
        &extra_code,
    );
}

pub fn card(appid: &str, size: u32, color: &palette::Srgb) {
    generate_card(
        size,
        size,
        &format!("public.new/icons/scalable/{}.svg", appid),
        (size as f64 * 0.875) as u32,
        color,
        &format!("public.new/icons/{}/{}.png", size, appid),
        "",
    );
}

pub fn generate_icon(icon: &str, size: u32, shade: f32, out_filename: &str) {
    let wrap_icon = format!(
        r##"<?xml version='1.0' encoding='UTF-8' standalone='no'?>

<svg version='1.1' width='{size}' height='{size}' xmlns='http://www.w3.org/2000/svg'>
    <defs>
        <filter id="colorize">
            <feColorMatrix
                values="
                    0 0 0 {shade} 0
                    0 0 0 {shade} 0
                    0 0 0 {shade} 0
                    0 0 0 1 0" />
        </filter>
    </defs>

    <g filter="url(#colorize)">{svg}</g>

</svg>"##,
        svg = load_sub_svg(icon, size, size),
        shade = shade,
        size = size,
    );
    render(&wrap_icon, size, size, out_filename);
}

pub fn darken(color: &palette::Srgb) -> palette::Srgb {
    palette::Srgb::from_linear(color.into_linear().darken(0.08))
}

pub fn lighten(color: &palette::Srgb) -> palette::Srgb {
    palette::Srgb::from_linear(color.into_linear().lighten(0.08))
}

fn generate_card(
    image_width: u32,
    image_height: u32,
    logo: &str,
    logo_size: u32,
    color: &palette::Srgb,
    out_filename: &str,
    extra_code: &str,
) {
    let color1 = lighten(color);
    let color2 = darken(color);

    let wrap_card = format!(
        r##"<?xml version='1.0'?>

<svg version='1.1' width='{image_width}' height='{image_height}' xmlns='http://www.w3.org/2000/svg'>
    <defs>
        <linearGradient id="gradient" x1="0" x2="1" y1="0" y2="0.2">
            <stop offset="0%" stop-color="{color1}" />
            <stop offset="100%" stop-color="{color2}" />
        </linearGradient>

        <filter id="shadow">
            <feDropShadow
                dx="{shadow_size_1}"
                dy="{shadow_size_1}"
                stdDeviation="{shadow_size_1}"
                flood-opacity="0.4" />
            <feDropShadow
                dx="{shadow_size_1}"
                dy="{shadow_size_2}"
                stdDeviation="{shadow_size_2}"
                flood-opacity="0.2" />
        </filter>
    </defs>

    <rect width="100%" height="100%" fill="url(#gradient)" />

    {extra_code}
    <g style="filter:url(#shadow);" transform="matrix(1, 0, 0, 1, {logo_x}, {logo_y})">{svg}</g>
</svg>
"##,
        svg = load_sub_svg(logo, logo_size, logo_size),
        image_width = image_width,
        image_height = image_height,
        logo_x = image_width / 2 - logo_size / 2,
        logo_y = image_height / 2 - logo_size / 2,
        shadow_size_1 = logo_size as f64 / 180.,
        shadow_size_2 = logo_size as f64 / 60.,
        extra_code = extra_code,
        color1 = crate::utils::to_css_color(&color1),
        color2 = crate::utils::to_css_color(&color2),
    );

    render(&wrap_card, image_width, image_height, out_filename);
}

fn render(svg_code: &str, width: u32, height: u32, filename: &str) {
    let bytes = gio::glib::Bytes::from_owned(svg_code.as_bytes().to_owned());
    let stream = gio::MemoryInputStream::from_bytes(&bytes);

    let handle = rsvg::Handle::from_stream_sync(
        &stream,
        gio::File::NONE,
        rsvg::HandleFlags::empty(),
        gio::Cancellable::NONE,
    )
    .unwrap()
    .unwrap();

    let surface =
        cairo::ImageSurface::create(cairo::Format::ARgb32, width as i32, height as i32).unwrap();
    let cr = cairo::Context::new(&surface).unwrap();

    handle
        .render_document(
            &cr,
            &rsvg::Rectangle::new(0., 0., width as f64, height as f64),
        )
        .unwrap();

    let mut file = std::fs::File::create(filename).unwrap();
    surface.write_to_png(&mut file).unwrap();
}

fn load_sub_svg(filename: &str, width: u32, height: u32) -> String {
    let mut element = xmltree::Element::parse(std::fs::File::open(filename).context(filename.to_owned()).unwrap()).unwrap();

    if !element.attributes.contains_key("viewBox") {
        element.attributes.insert(
            String::from("viewBox"),
            format!(
                "0 0 {} {}",
                element.attributes.get("width").unwrap(),
                element.attributes.get("height").unwrap()
            ),
        );
    }

    *element.attributes.entry("width".to_string()).or_default() = format!("{}", width);
    *element.attributes.entry("height".to_string()).or_default() = format!("{}", height);

    let mut buf = Vec::new();
    element
        .write_with_config(
            &mut buf,
            xmltree::EmitterConfig::new().write_document_declaration(false),
        )
        .unwrap();

    String::from_utf8_lossy(&buf).to_string()
}

pub fn dominant_colors(app_id: &AppId) -> Vec<palette::Srgb> {
    let icon = bytes_64x64(format!("public.new/icons/scalable/{app_id}.svg",)).unwrap();
    let colors = dominant_color::get_colors(&icon, true);
    let colors: Vec<palette::Srgb> = colors
        .into_iter()
        .tuples()
        .map(|(r, g, b, _)| {
            palette::Srgb::new(r as f32 / 255.0, g as f32 / 255.0, b as f32 / 255.0)
        })
        .collect();

    debug!("Colors: {:?}", colors);

    colors
}

fn bytes_64x64<P: AsRef<Path>>(path: P) -> color_eyre::Result<Vec<u8>> {
    let handle = rsvg::Handle::from_file(&path.as_ref().to_string_lossy())
        .context(format!("{:?}", path.as_ref()))?
        .unwrap();
    let surface = cairo::ImageSurface::create(cairo::Format::ARgb32, 64, 64)?;
    let cr = cairo::Context::new(&surface)?;
    handle.render_document(&cr, &rsvg::Rectangle::new(0., 0., 64., 64.))?;
    drop(cr);

    let data = surface.take_data()?;
    let mut slice = data.as_ref();

    let mut bytes = Vec::new();
    let mut pixel = vec![0; 4];
    while slice.read_exact(&mut pixel).is_ok() {
        // convert from BGRA to RGBA
        if pixel[3] > 0 {
            bytes.push(pixel[2]);
            bytes.push(pixel[1]);
            bytes.push(pixel[0]);
            bytes.push(pixel[3]);
        }
    }

    Ok(bytes)
}
