use page_tools::{Apps, AppsMinimal, Components};
use tokio::io::AsyncWriteExt;

use crate::collect::{people, AppRaw, GnomeProject};
use crate::context::Out;
use crate::utils::new_file_tokio;

pub async fn generate_apps(apps: &[AppRaw]) -> color_eyre::Result<()> {
    let data = Vec::from(apps).out("");
    let apps = Apps::from(data);

    let json_data = serde_json::to_string_pretty(&apps)?;

    let mut file = new_file_tokio("_api/apps.json").await?;

    file.write_all(json_data.as_bytes()).await?;

    Ok(())
}

pub async fn generate_minimal(apps: AppsMinimal) -> color_eyre::Result<()> {
    let json_data = serde_json::to_string_pretty(&apps)?;
    let path = std::path::PathBuf::from_iter(["_api", apps.lang(), AppsMinimal::FILENAME]);

    let mut file = new_file_tokio(path).await?;

    file.write_all(json_data.as_bytes()).await?;

    Ok(())
}

pub async fn generate_official_gnome_apps(apps: &[AppRaw]) -> color_eyre::Result<()> {
    let mut official_apps: Vec<_> = apps
        .iter()
        .filter_map(|app| {
            if matches!(
                app.project,
                GnomeProject::CoreApps | GnomeProject::CoreDeveloperTools
            ) {
                Some(&app.id)
            } else {
                None
            }
        })
        .collect();

    official_apps.sort();

    let json_data = serde_json::to_string_pretty(&official_apps)?;

    let mut file = new_file_tokio("_api/official-gnome-apps.json").await?;

    file.write_all(json_data.as_bytes()).await?;

    Ok(())
}

pub async fn generate_people() -> color_eyre::Result<()> {
    let json_data = serde_json::to_string_pretty(&people::people())?;
    let path = std::path::PathBuf::from_iter(["_api", "people.json"]);

    let mut file = new_file_tokio(path).await?;

    file.write_all(json_data.as_bytes()).await?;

    Ok(())
}

pub async fn generate_components(components: &Components) -> color_eyre::Result<()> {
    let json_data = serde_json::to_string_pretty(components)?;
    let path = std::path::PathBuf::from_iter(["_api", "components.json"]);

    let mut file = new_file_tokio(path).await?;

    file.write_all(json_data.as_bytes()).await?;

    Ok(())
}
