use std::collections::{BTreeSet, HashMap};
use std::fmt::Display;
use std::sync::Mutex;

use arc_swap::ArcSwap;
use libappstream::prelude::*;
use once_cell::sync::Lazy;

pub static LOG: Lazy<ArcSwap<HashMap<String, BTreeSet<page_tools::Annotation>>>> =
    Lazy::new(Default::default);
pub static TIMED: Lazy<Mutex<Option<std::time::Instant>>> = Lazy::new(Default::default);

pub static LOG_SENDER: Lazy<std::sync::mpsc::Sender<(String, String)>> = Lazy::new(|| {
    let (sender, recv) = std::sync::mpsc::channel();

    std::thread::spawn(move || {
        while let Ok((app_id, msg)) = recv.recv() {
            let is_new_message = add_log_entry(&app_id, "warn", &msg);

            if is_new_message {
                warn!("{}: {}", app_id, msg);
            }
        }
    });

    sender
});

pub fn add_log_entry(app_id: impl Display + Clone, level: &str, msg: impl Display) -> bool {
    let mut res = true;
    LOG.rcu(|current| {
        let mut new = (**current).clone();
        res = new
            .entry(crate::context::real_id(app_id.clone()).to_string())
            .or_default()
            .insert(page_tools::Annotation {
                level: level.to_string(),
                msg: msg.to_string(),
            });
        new
    });
    res
}

#[macro_export]
macro_rules! log_warn {
    ($app:expr, $($arg:tt)+) => ({
        let app_string = $app.to_string();
        // Remove prefix if passing a ProjectId
        let app_id = app_string.strip_prefix("app:").unwrap_or(app_string.as_str());

        let msg = format!($($arg)+);
        $crate::error::LOG_SENDER.send((app_id.to_string(), msg)).unwrap();
    })
}

#[macro_export]
macro_rules! timed_info {
    ($($arg:tt)+) => ({
        let msg = format!($($arg)+);
        if let Some(x) = *$crate::error::TIMED.lock().unwrap() {
            info!("{} ms needed for completion", x.elapsed().as_millis());
        }
        *$crate::error::TIMED.lock().unwrap() = Some(std::time::Instant::now());
        info!("{}", msg);
    })
}

pub trait LogError {
    fn warn<C>(self, context: C) -> Self
    where
        C: std::fmt::Display;
}

impl<T, E> LogError for Result<T, E>
where
    E: std::fmt::Display,
{
    fn warn<C>(self, context: C) -> Self
    where
        C: std::fmt::Display,
    {
        if let Err(msg) = &self {
            log_warn!(context, "{}", msg)
        }

        self
    }
}

#[derive(Debug)]
pub struct AppError {
    app: String,
    msg: String,
}

impl AppError {
    pub fn from_app(app: &libappstream::Component, msg: &str) -> Self {
        warn!("{}: {}", app.id().unwrap().to_string(), msg);
        Self {
            app: app.id().unwrap().to_string(),
            msg: msg.to_string(),
        }
    }
}

impl std::fmt::Display for AppError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "AppError '{}': {}", self.app, self.msg)
    }
}

impl std::error::Error for AppError {}
