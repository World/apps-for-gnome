use serde::Deserialize;

pub type Document = std::collections::HashMap<String, Project>;

pub fn gitlab_map(doc: &Document) -> std::collections::HashMap<String, String> {
    let mut map = std::collections::HashMap::new();

    for project in doc.values() {
        for maintainer in &project.maintainers {
            if let (Some(ldap), Some(gitlab)) = (
                maintainer.ldap_username.clone(),
                maintainer.gitlab_username.clone(),
            ) {
                map.insert(ldap, gitlab);
            }
        }
    }

    map
}

#[derive(Debug, Deserialize, Clone)]
pub struct Project {
    maintainers: Vec<Maintainer>,
}

#[derive(Debug, Deserialize, Clone)]
pub struct Maintainer {
    gitlab_username: Option<String>,
    ldap_username: Option<String>,
}
