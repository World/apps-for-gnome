# Apps for GNOME

This repository manages the code and coordination for the [apps.gnome.org](https://apps.gnome.org) website.

- [The website](https://apps.gnome.org)
- [Translation on “damned lies”](https://l10n.gnome.org/module/apps-for-gnome/)
- [Tweaking the appearance of your app (METADATA.md)](METADATA.md)
- [More information on contributing (CONTRIBUTING.md)](CONTRIBUTING.md)

## APIs

Apps for GNOME also provides several APIs to access the collected data. The APIs consist of static JSON files that are updated nightly together with the pages. They are available under `https://apps.gnome.org/_api/…`. The [gnome-page-tools](gnome-page-tools/) provide rust implementations to access these APIs. There is also convenience functions available to retrieve the data like `gnome_page_tools::Apps::from_upstream_cached()`.

- [`_api/apps.json`](https://apps.gnome.org/_api/apps.json) List of all Core, Circle, Development, and, Incubator apps. All translatable parts are in English.
- [`_api/<lang>/apps-minimal.json`](https://apps.gnome.org/_api/fa/apps-minimal.json) Minimal version of `apps.json` but translated. [Welcome to GNOME](https://welcome.gnome.org/) uses this for app search besides other things.
- [`_api/official-gnome-apps.json`](https://apps.gnome.org/_api/official-gnome-apps.json)
- [`_api/people.json`](https://apps.gnome.org/_api/people.json) Experimental API currently only listing app and Core library maintainers.
- [`_api/components.json`](https://apps.gnome.org/_api/components.json) Experimental API currently only listing Core library maintainers.

## Helpful blog posts

1. [Project introduction: *An “Apps for GNOME” website*](https://blogs.gnome.org/sophieh/2021/08/05/apps-for-gnome/)
2. [How to optimize your app's metadata: *Get set: Apps for GNOME on its mark*](https://blogs.gnome.org/sophieh/2021/08/15/apps-for-gnome-on-its-mark/)
3. [Project goes online: *“apps.gnome.org” is Online!*](https://blogs.gnome.org/sophieh/2021/08/26/apps-gnome-org-is-online/)

## Media coverage

1. [The Linux Experiment, Linux news August (Aug. 2021)](https://www.youtube.com/watch?v=vU7m1UxFt9M)
2. [Phoronix, *"Apps For GNOME" Launches To Highlight GNOME Apps* (Aug. 2021)](https://www.phoronix.com/scan.php?page=news_item&px=Apps-For-GNOME)
3. [The Register, *'Apps for GNOME' site aims to improve discovery of the project's best applications* (Aug. 2021)](https://www.theregister.com/2021/08/27/apps_for_gnome/)
4. [OMG! Ubuntu!, *New Website Launched to Showcase GNOME Apps* (Aug. 2021)](https://www.omgubuntu.co.uk/2021/08/new-website-launched-to-showcase-gnome-apps)
5. [LINUX Unplugged, Episode 421 (Aug. 2021)](https://linuxunplugged.com/421)
6. [The Linux Experiment, Linux News August 2021 (Sept. 2021)](https://www.youtube.com/watch?v=ZK3pDv4HPJQ)
