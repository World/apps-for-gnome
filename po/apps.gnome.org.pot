msgid ""
msgstr ""
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

msgid "About Us"
msgstr ""

msgid "App is translated"
msgstr ""

msgid "App specific content © The respective app contributors"
msgstr ""

msgid "App supports mobile devices"
msgstr ""

#. Translators: CHANGED STRING, WILL BE USED SOON: Main page introductory text (markdown formatted)
msgid "Apps featured in this curated overview are all built with the GNOME philosophy in mind. They are easy to understand and simple to use, feature a consistent and polished design and provide a noticeable attention to details. Naturally, they are [free software](https://fsfe.org/freesoftware/) and have committed to being part of a [welcoming and friendly community](https://conduct.gnome.org/). These apps will perfectly integrate with your [GNOME Desktop](https://www.gnome.org/)."
msgstr ""

#. Translators: LEGACY STRING, WILL BE REMOVED SOON: Main page introductory text (markdown formatted)
msgid "Apps featured in this curated overview are all built with the GNOME philosophy in mind. They are easy to understand and simple to use, feature a consistent and polished design and provide a noticeable attention to details. Naturally, they are [free software](https://fsfe.org/freesoftware/) and have committed to being part of a [welcoming and friendly community](https://wiki.gnome.org/Foundation/CodeOfConduct). These apps will perfectly integrate with your [GNOME Desktop](https://www.gnome.org/)."
msgstr ""

#. Translators: Page title and main heading
msgid "Apps for GNOME"
msgstr ""

msgid "Apps supported on GNOME mobile devices are marked with the mobile icon."
msgstr ""

#. Translators: Short for 'GNOME Circle apps.' Navigation link to app section.
msgid "Circle"
msgstr ""

#. Translators: App list section heading
msgid "Circle Apps"
msgstr ""

msgid "Close Language Selection"
msgstr ""

msgid "Contact Us"
msgstr ""

msgid "Contribute"
msgstr ""

msgid "Contribute your ideas or report issues on the app’s issue tracker."
msgstr ""

#. Translators: Short for 'GNOME Core apps.' Navigation link to app section.
msgid "Core"
msgstr ""

#. Translators: App list section heading
msgid "Core Apps"
msgstr ""

#. Translators: Short for 'GNOME Development tools.' Navigation link to app section.
msgid "Development"
msgstr ""

#. Translators: App list section heading
msgid "Development Tools"
msgstr ""

msgid "Discover the best Apps for GNOME"
msgstr ""

#. Translators: Page subtitle
msgid "Discover the best applications in the GNOME ecosystem and learn how to get involved."
msgstr ""

msgid "Donate money"
msgstr ""

msgid "Donate to the GNOME Foundation to support this project."
msgstr ""

#. Translators: App details section headings
msgid "Explore the interface"
msgstr ""

msgid "FAQ"
msgstr ""

msgid "Find answers to the most frequently asked questions."
msgstr ""

#. Translators: App list section text (markdown formatted)
msgid "GNOME Circle contains applications extending the GNOME ecosystem. It champions the great additional software that is available for the GNOME platform. [Learn more about GNOME Circle.](https://circle.gnome.org/)"
msgstr ""

#. Translators: App list section text
msgid "GNOME Core apps cover ordinary tasks on the GNOME desktop. They are usually pre-installed on your GNOME system."
msgstr ""

#. Translators: App list section text
msgid "GNOME Development tools help develop and design new apps and make it easy to contribute to existing ones."
msgstr ""

msgid "Get in touch"
msgstr ""

#. Translators: App details section headings
msgid "Get involved"
msgstr ""

msgid "Get the app"
msgstr ""

#. Translators: App details section heading
msgid "Get to know us"
msgstr ""

msgid "GitHub account"
msgstr ""

msgid "GitLab account"
msgstr ""

msgid "Give feedback"
msgstr ""

msgid "Help translate this app into your language."
msgstr ""

msgid "Help translating"
msgstr ""

msgid "Install the latest version from Flathub."
msgstr ""

msgid "Just chat with the app's community or get help when you are stuck."
msgstr ""

msgid "Keywords"
msgstr ""

#. Translators: This is a template string. You can move a pair of curly braces with the keyword in between around. But do not change or translate the keyword in between the braces.
msgid "Latest version { version_name } released on { date }."
msgstr ""

msgid "Learn how you can contact the project."
msgstr ""

msgid "Learn how you can contribute to this app."
msgstr ""

msgid "Learn how you can donate to the developers of this project."
msgstr ""

msgid "Learn more"
msgstr ""

msgid "Maintainer"
msgstr ""

#. Translators: App details section heading
msgid "More Information"
msgstr ""

msgid "Newest Release"
msgstr ""

msgid "Not translated"
msgstr ""

msgid "Only a small part of this app is available in your language."
msgstr ""

msgid "Overview"
msgstr ""

msgid "Page Software"
msgstr ""

msgid "Page generated"
msgstr ""

msgid "Partially translated"
msgstr ""

msgid "Privacy"
msgstr ""

msgid "Project homepage"
msgstr ""

msgid "Releases"
msgstr ""

msgid "Select Language"
msgstr ""

msgid "Some parts of this app are available in your language."
msgstr ""

msgid "Sparsely translated"
msgstr ""

msgid "Switch Language"
msgstr ""

msgid "The GNOME Project"
msgstr ""

msgid "This app is available in your language."
msgstr ""

msgid "This app is not available in your language."
msgstr ""

msgid "This website is available in many languages"
msgstr ""

msgid "Visit the dedicated homepage for this project."
msgstr ""

msgid "Visit the online help page for this app."
msgstr ""

msgid "Website"
msgstr ""

#. Translators: Information about seasonal header image
msgid "[Disability Pride Month](https://en.wikipedia.org/wiki/Disability_Pride_Month), displaying two [disability flags](https://en.wikipedia.org/wiki/Disability_flag) and an [autistic flag](https://en.wikipedia.org/wiki/Autistic_Pride_Day)."
msgstr ""

#. Translators: Information about seasonal header image
msgid "[Queer Pride Month](https://en.wikipedia.org/wiki/Pride_Month), displaying the [intersex](https://en.wikipedia.org/wiki/Intersex_flag), [trans](https://en.wikipedia.org/wiki/Transgender_flag), and [rainbow](https://en.wikipedia.org/wiki/Rainbow_flag_(LGBT)), flags."
msgstr ""

#. Translators: The {} will be replaced with a social media name like 'Mastodon'
msgid "{} account"
msgstr ""

msgid "© The “Apps for GNOME” contributors"
msgstr ""

